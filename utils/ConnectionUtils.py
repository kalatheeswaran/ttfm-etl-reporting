import json
import mysql.connector
import mysql.connector.pooling


class ConnectionUtils:
    @staticmethod
    def get_connection_survey_db():
        try:
            with open('resources/config/dbConfigSurveyDB.json', 'r') as dbConfig:
                data = json.load(dbConfig)
                dict_conn = {}

                if data['env'] == 'dev':
                    dict_conn = data['dev']
                elif data['env'] == 'testing-ca':
                    dict_conn = data['testing-ca']
                elif data['env'] == 'prod':
                    dict_conn = data['prod']

                db = mysql.connector.connect(host=dict_conn['host'], user=dict_conn['user'],
                                             passwd=dict_conn['password'], database=dict_conn['database'],
                                             port=dict_conn['port'], pool_name='connPoolSurveyDB', pool_size=30)
                return db

        except Exception as error:
            print('An error occurred : ', error)
            return None

    @staticmethod
    def get_connection_reporting_db():
        try:
            rep_db_conn = None
            with open('resources/config/dbConfigReportingDB.json', 'r') as dbConfig:
                data = json.load(dbConfig)
                dict_conn = {}

                if data['env'] == 'dev':
                    dict_conn = data['dev']
                elif data['env'] == 'testing-ca':
                    dict_conn = data['testing-ca']
                elif data['env'] == 'prod':
                    dict_conn = data['prod']

                if rep_db_conn is None:
                    rep_db_conn = mysql.connector.connect(host=dict_conn['host'], user=dict_conn['user'],
                                                          passwd=dict_conn['password'], database=dict_conn['database'],
                                                          port=dict_conn['port'], pool_name='connPoolReportingDB',
                                                          pool_size=30)
                return rep_db_conn

        except Exception as error:
            print('An error occurred : ', error)
            return None

    def run_select_query_survey_db(self, sql):
        db = None
        try:
            db = self.get_connection_survey_db()

            if db is not None:
                db_cursor = db.cursor(dictionary=True)
                db_cursor.execute(sql)
                db_result = db_cursor.fetchall()
                return db_result

        except Exception as error:
            print('An error occurred : ', error)

        finally:
            if db is not None:
                db.close()
                print('DB connection closed in run_select_query_survey_db()')
            else:
                print('DB is None in run_select_query_survey_db')

    def run_select_query_reporting_db(self, sql):
        db = None
        try:
            db = self.get_connection_reporting_db()

            if db is not None:
                db_cursor = db.cursor(dictionary=True)
                db_cursor.execute(sql)
                db_result = db_cursor.fetchall()
                return db_result

        except Exception as error:
            print('An error occurred : ', error)

        finally:
            if db is not None:
                db.close()
                print('DB connection closed in run_select_query_reporting_db()')
            else:
                print('DB is None in run_select_query_reporting_db')

    def run_single_insert_query_reporting_db(self, sql, value):
        db = None
        try:
            db = self.get_connection_reporting_db()
            db_cursor = db.cursor(dictionary=True)
            db_cursor.execute(sql, value)
            db.commit()

        except Exception as error:
            print('An error occurred : ', error)

        finally:
            if db is not None:
                db.close()
                print('DB connection closed in run_single_insert_query_reporting_db()')
            else:
                print('DB is None in run_single_insert_query_reporting_db')

    def run_multiple_insert_query_reporting_db(self, sql, values):
        db = None
        try:
            db = self.get_connection_reporting_db()
            db_cursor = db.cursor(dictionary=True)
            db_cursor.executemany(sql, values)
            db.commit()

        except Exception as error:
            print('An error occurred : ', error)

        finally:
            if db is not None:
                db.close()
                print('DB connection closed in run_multiple_insert_query_reporting_db()')
            else:
                print('DB is None in run_multiple_insert_query_reporting_db')

    def run_delete_query_reporting_db(self, sql, value, table_name):
        db = None
        try:
            db = self.get_connection_reporting_db()
            db_cursor = db.cursor(dictionary=True)
            db_cursor.execute(sql, (value,))
            db.commit()
            print(db_cursor.rowcount, 'record(s) were deleted from', table_name)

        except Exception as error:
            print('An error ', error, ' occurred in sql-> ', sql, ' value -> ', value)

        finally:
            if db is not None:
                db.close()
                print('DB connection closed in run_delete_query_reporting_db()')
            else:
                print('DB is None in run_delete_query_reporting_db')





