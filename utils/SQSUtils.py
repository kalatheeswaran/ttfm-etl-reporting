import boto3


class SQSUtils:
    sqs = boto3.resource('sqs')
    client = boto3.client('sqs')

    # function to create a queue
    def create_queue(self, queue_name):
        res = None
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            # print(lst_queue.get('QueueUrls'))
            if lst_queue.get('QueueUrls') is None:
                res = self.client.create_queue(QueueName=queue_name)
                print('Queue is created with url:', res)
            else:
                res = self.client.get_queue_url(QueueName=queue_name)
                print('Queue already exists with url:', res)        
        except Exception as error:
            print('An error occurred while creating a queue:', error)
        finally:
            return res

    # function to delete a queue
    def delete_queue(self, queue_name):
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            if lst_queue.get('QueueUrls') is None:
                print('Queue', queue_name, 'is not available.')
            else:
                queue = self.sqs.get_queue_by_name(QueueName=queue_name)
                queue_url = queue.url
                self.client.delete_queue(QueueUrl=queue_url)
                print('Queue', queue_name, 'has been deleted.')
        except Exception as error:
            print('An error occurred while deleting the queue: ', queue_name, ': ', error)

    # function to delete all the messages in a queue
    def purge_queue(self, queue_name):
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            if lst_queue.get('QueueUrls') is None:
                print('Queue', queue_name, 'is not available.')
            else:
                queue = self.sqs.get_queue_by_name(QueueName=queue_name)
                queue_url = queue.url
                self.client.purge_queue(QueueUrl=queue_url)
                print('Queue', queue_name, 'has been purged of all message(s).')
        except Exception as error:
            print('An error occurred while purging message(s) in queue:', queue_name, ': ', error)

    # function to send message to queue from a list
    def send_message(self, queue_name, lst_json):
        res = None
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            if lst_queue.get('QueueUrls') is None:
                print('Queue', queue_name, 'is not available.')
            elif lst_json is None:
                print('Message list lstJson is empty.')
            else:
                queue = self.sqs.get_queue_by_name(QueueName=queue_name)
                for msg in lst_json:
                    # print(msg)
                    res = queue.send_message(MessageBody=msg)
                    http_status_code = res["ResponseMetadata"]["HTTPStatusCode"]
                    # print('http_status_code => ', http_status_code)
                    while http_status_code != 200:
                        res = queue.send_message(MessageBody=msg)
                        http_status_code = res["ResponseMetadata"]["HTTPStatusCode"]
                        print('Message resent:', msg, 'http_status_code => ', http_status_code)
                    # print('Message(s) sent to queue:', queue_name)

        except Exception as error:
            print('An error occurred while sending message(s) to queue:', queue_name, ': ', error)

        finally:
            return res

    # function to send message to queue from a list
    def send_message_in_batch(self, queue_name, lst_json):
        res = None
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            if lst_queue.get('QueueUrls') is None:
                print('Queue', queue_name, 'is not available.')
            elif lst_json is None:
                print('Message list lstJson is empty.')
            else:
                queue = self.sqs.get_queue_by_name(QueueName=queue_name)
                res = self.client.send_message_batch(QueueUrl=queue.url, Entries=lst_json)
                http_status_code = res["ResponseMetadata"]["HTTPStatusCode"]

                while http_status_code != 200:
                    res = self.client.send_message_batch(QueueUrl=queue.url, Entries=lst_json)
                    http_status_code = res["ResponseMetadata"]["HTTPStatusCode"]
                    print('Message resent:', lst_json, 'http_status_code => ', http_status_code)

        except Exception as error:
            print('An error occurred while sending message(s) to queue:', queue_name, ': ', error)

        finally:
            return res

    # function to retrieve messages from a queue
    def retrieve_message(self, queue_name, flag_delete):
        msg_body = None
        try:
            lst_queue = self.client.list_queues(QueueNamePrefix=queue_name)
            if lst_queue.get('QueueUrls') is None:
                print('Queue', queue_name, 'is not available.')
            else:
                queue = self.sqs.get_queue_by_name(QueueName=queue_name)

                res = self.client.receive_message(
                    QueueUrl=queue.url,
                    AttributeNames=['SentTimestamp'],
                    MaxNumberOfMessages=1,
                    MessageAttributeNames=['All'],
                    VisibilityTimeout=0,
                    WaitTimeSeconds=20
                )

                if 'Messages' in res:
                    msg = res['Messages'][0]
                    msg_body = msg['Body']
                    if flag_delete is True:
                        receipt_handle = msg['ReceiptHandle']
                        self.client.delete_message(
                            QueueUrl=queue.url,
                            ReceiptHandle=receipt_handle
                        )
                        print('Received message:', msg_body, 'and deleted it')
                    else:
                        pass
                        # print('Received message:', msg_body)
                else:
                    print('Queue', queue_name, 'is empty')

        except Exception as error:
            print('An error occurred while retrieving message(s) from queue:', queue_name, ': ', error)

        finally:
            return msg_body
