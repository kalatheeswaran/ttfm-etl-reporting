End point URLs:
---------------

1) https://vjgcqdj0vb.execute-api.ca-central-1.amazonaws.com/testing-ca/main
2) https://vjgcqdj0vb.execute-api.ca-central-1.amazonaws.com/testing-ca/etlRepSurveyInstance
3) https://vjgcqdj0vb.execute-api.ca-central-1.amazonaws.com/testing-ca/etlRepParticipantData
4) https://vjgcqdj0vb.execute-api.ca-central-1.amazonaws.com/testing-ca/etlRepStoreParticipantMeasureScore
5) https://vjgcqdj0vb.execute-api.ca-central-1.amazonaws.com/testing-ca/etlRepStoreParticipantQnScore
