import json
import sys
import controller.EtlRepSurveyInstanceController as siCtrl
import controller.EtlRepParticipantDataController as partCtrl
import controller.EtlRepStoreParticipantMeasureScoreController as storePmsCtrl
import controller.EtlRepStoreParticipantQuestionScoreController as storePqsCtrl
import controller.EtlRepSchoolDataController as schCtrl
from datetime import datetime

si_ctrl_obj = siCtrl.EtlRepSurveyInstanceController()
part_ctrl_obj = partCtrl.EtlRepParticipantDataController()
store_pms_ctrl_obj = storePmsCtrl.EtlRepStoreParticipantMeasureScoreController()
store_pqs_ctrl_obj = storePqsCtrl.EtlRepStoreParticipantQuestionScoreController()
sch_ctrl = schCtrl.EtlRepSchoolDataController()


def return_function(event, function_name):
    body = {
        "message": "Your function " + function_name + " executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response


def main(event, context):
    print('In main of handler.py')
    res = return_function(event, 'main')
    return res


def etl_rep_survey_instance(event, context):
    print('In etlRepSurveyInstance of handler.py')
    si_ctrl_obj.entry_function()
    print('Done etlRepSurveyInstance')
    res = return_function(event, 'etlRepSurveyInstance')
    return res


def etl_rep_participant_data(event, context):
    print('In etlRepParticipantData of handler.py')
    si = event['Records'][0]['body']
    part_ctrl_obj.entry_function(si)
    print('Done etlRepParticipantData')
    res = return_function(event, 'etlRepParticipantData')
    return res


def etl_rep_store_participant_measure_score(event, context):
    print('In etlRepStoreParticipantMeasureScore of handler.py')
    pms_data = event['Records'][0]['body']
    store_pms_ctrl_obj.entry_function(pms_data)
    print('Done etlRepStoreParticipantMeasureScore')
    res = return_function(event, 'etlRepStoreParticipantMeasureScore')
    return res


def etl_rep_store_participant_question_score(event, context):
    print('In etlRepStoreParticipantQnScore of handler.py')
    pqs_data = event['Records'][0]['body']
    store_pqs_ctrl_obj.entry_function(pqs_data)
    print('Done etlRepStoreParticipantQnScore')
    res = return_function(event, 'etlRepStoreParticipantQnScore')
    return res


if __name__ == "__main__":
    param = sys.argv[1]
    if param == 'main':
        main('', '')

    elif param == 'etlRepSurveyInstance':
        now1 = datetime.now()
        si_ctrl_obj.entry_function()
        now2 = datetime.now()
        print("Total time for etlRepSurveyInstance => ", now2 - now1)

    elif param == 'etlRepParticipantData':
        now1 = datetime.now()
        part_ctrl_obj.entry_function(None)
        now2 = datetime.now()
        print("Total time for etlRepParticipantData => ", now2 - now1)

    elif param == 'etlRepStoreParticipantMeasureScore':
        store_pms_ctrl_obj.entry_function(None)

    elif param == 'etlRepStoreParticipantQnScore':
        store_pqs_ctrl_obj.entry_function(None)

    elif param == 'etlRepSchoolData':
        sch_ctrl.entry_function()

    else:
        print('Invalid argument! Pass in a valid argument!')
