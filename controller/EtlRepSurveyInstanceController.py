import json
import controller.CommonController as cc
import constants.SQLConstants as sc
import constants.FileConstants as fc
import utils.CheckEmptyUtils as ceu
import constants.DimTargetGroupConstants as dtgc
import traceback
from itertools import combinations


class EtlRepSurveyInstanceController(cc.CommonController, sc.SQLConstants):
    # entry function calls the get_batch_survey_instances(), write_to_dim_survey_instance()
    # and sends the messages to anaSqsSurveyInstance queue
    def entry_function(self):
        self.get_batch_survey_instances()

    # runs the batch survey instances query and returns the list of survey instances in a json format
    def get_batch_survey_instances(self):
        max_instances = self.sqs_obj.retrieve_message('tmpMaxInstances', True)
        print('max_instances -> ', max_instances)
        for x in range(int(max_instances)):
            si = self.sqs_obj.retrieve_message('tmpSqsSurveyInstance', True)

            if si is not None:
                si = json.loads(si)
                self.process_dim_survey_instance(si.get('surveyInstanceId'), si.get('dimTargetGroupId'))
                si = self.get_dim_survey_instance(si.get('surveyInstanceId'))

                dict_si = {
                    'dimSurveyInstanceId': si[0]['id'],
                    'surveyInstanceId': si[0]['surveyInstanceId'],
                    'dimOrgUnitId': si[0]['dimOrgUnitId'],
                    'orgId': si[0]['orgId'],
                    'surveyDefinitionId': si[0]['surveyDefinitionId'],
                    'dimTargetGroupId': si[0]['dimTargetGroupId'],
                    'yearDefId': si[0]['yearDefId'],
                    'dimBrandId': si[0]['dimBrandId'],
                    'brandPropertiesKey': si[0]['brandPropertiesKey']
                }

                lst_survey_instance = list()
                lst_survey_instance.append(json.dumps(dict_si))
                res = self.sqs_obj.send_message('etlRepSqsSurveyInstance', lst_survey_instance)
                # print('res -> ', res)
                # lst_msg_to_snd = ["i1", "i2", "i3", "i4", "i5", "i6"]
                # res = self.sqs_obj.send_message('etlRepSqsSurveyInstance', lst_msg_to_snd)
            else:
                print('Survey Instance is none! Please check!')

    # checks if the survey instance exists in the dim_survey_instance table. If so associated entries are deleted from
    # fact_student_measure_score, fact_student_question_likert_score, dim_survey_participant, dim_survey_instance tables
    # for re-computation purposes. A new entry is made in the dim_survey_instance table
    def process_dim_survey_instance(self, survey_instance_id, dim_target_group_id):
        flag = False
        try:
            print('Processing si => ', survey_instance_id)
            dim_si = self.get_dim_survey_instance(survey_instance_id)
            if self.empty_utils_obj.is_empty(dim_si):
                flag = self.add_dim_survey_instance_into_tables(survey_instance_id, dim_target_group_id)
            else:
                dim_survey_instance_id = dim_si[0]['id']
                flag = self.delete_dim_survey_instance_entries(dim_survey_instance_id)
                if flag is True:
                    flag = self.add_dim_survey_instance_into_tables(survey_instance_id, dim_target_group_id)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
            flag = False

        finally:
            return flag

    def delete_dim_survey_instance_entries(self, dim_survey_instance_id):
        flag = False
        try:
            sql = 'Delete From fact_school_measure_score Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_school_measure_score')

            sql = 'Delete From fact_school_question_likert Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_school_question_likert')

            sql = 'Delete From fact_student_measure_score Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_student_measure_score')

            sql = 'Delete From fact_student_question_likert_score Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_student_question_likert_score')

            sql = 'Delete From fact_student_oeq Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_student_oeq')

            sql = 'Delete From fact_student_mcq Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_student_mcq')

            sql = 'Delete From dim_survey_participant Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'dim_survey_participant')

            sql = 'Delete From bridge_mcq Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'bridge_mcq')

            sql = 'Delete From drc_mcq Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'drc_mcq')

            sql = 'Delete From student_question_recode Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'student_question_recode')

            sql = 'Delete From survey_instance_measure_report Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'survey_instance_measure_report')

            sql = 'Delete From survey_instance_measure Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'survey_instance_measure')

            sql = 'Delete From survey_instance_measure_question Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'survey_instance_measure_question')

            # sql = 'Delete From dim_custom_measure Where id = %s'
            # self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'dim_custom_measure')
            #
            # sql = 'Delete From drc_survey_instance Where id = %s'
            # self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'drc_survey_instance')

            sql = 'Delete From dim_survey_instance Where id = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'dim_survey_instance')

            flag = True
        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
            flag = False
        finally:
            return flag

    # gets the id from dim_organizational_unit table and inserts the data into dim_survey_instance table after
    # running through the survey instance details query
    def add_dim_survey_instance_into_tables(self, survey_instance_id, dim_target_group_id):
        flag = False
        try:
            sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSurveyInstanceDetails)
            sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))

            si_details = self.cu_obj.run_select_query_survey_db(sql)
            dim_org_id = self.get_dim_org_id(si_details)

            self.add_dim_survey_instance(si_details[0], dim_org_id)
            dim_si = self.get_dim_survey_instance(str(si_details[0]['id']))

            sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSurveyInstanceMeasures)
            sql = sql.replace('@pSurveyInstanceId', str(si_details[0]['id']))
            lst_si_measure = self.cu_obj.run_select_query_survey_db(sql)

            measure_ids = None
            for si_measure in lst_si_measure:
                if measure_ids is None:
                    measure_ids = str(si_measure['measureId'])
                else:
                    measure_ids = measure_ids + fc.FileConstants.COMMA_DELIMITER + str(si_measure['measureId'])

            if dim_target_group_id in [dtgc.DimTargetGroupConstants.SECONDARY_SURVEY, dtgc.DimTargetGroupConstants.ELEMENTARY_SURVEY]:
                measure_ids = str(measure_ids) + fc.FileConstants.COMMA_DELIMITER + "1055,1056,1057"

            lst_dim_measure = self.get_dim_measures(measure_ids)

            self.add_si_measure(lst_dim_measure, dim_si[0]['id'])
            self.add_si_measure_question(dim_si[0]['id'])
            # self.add_drc_question(dim_si[0]['surveyInstanceId'], 1, dim_si[0])

            lst_dim_qn = self.add_multiple_choice_answer(survey_instance_id, dim_si[0]['id'], lst_dim_measure)
            if not ceu.CheckEmptyUtils.is_empty(lst_dim_qn) or lst_dim_qn is not None:
                self.add_bridge_mcq(survey_instance_id, dim_si[0]['id'], lst_dim_qn)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
            flag = False

        finally:
            return flag

    def get_dim_target_group(self, target_group_id):
        dim_target_group = None
        try:
            sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimTargetGroup)
            sql = sql.replace('@pTargetGroupId', str(target_group_id))
            dim_target_group = self.cu_obj.run_select_query_reporting_db(sql)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return dim_target_group

    def get_dim_brand(self, brand_id):
        dim_brand = None
        try:
            sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimBrand)
            sql = sql.replace('@pBrandId', str(brand_id))
            dim_brand = self.cu_obj.run_select_query_reporting_db(sql)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return dim_brand

    def get_dim_org_id(self, si_details):
        dim_org_unit_id = None
        try:
            sql1 = "Select id From dim_organizational_unit Where schoolId = @pSchoolId And provinceId = @pProvinceId " \
                   "And districtId = @pDistrictId And countryId = @pCountryId"

            sql1 = sql1.replace('@pSchoolId', str(si_details[0]['schoolId'])) \
                .replace('@pDistrictId', str(si_details[0]['districtId'])) \
                .replace('@pProvinceId', str(si_details[0]['provinceId'])) \
                .replace('@pCountryId', str(si_details[0]['countryId']))

            dim_org_id = self.cu_obj.run_select_query_reporting_db(sql1)

            if not dim_org_id or dim_org_id is None:
                sql2 = "Select MAX(version) as maxVersion From dim_organizational_unit Where schoolId = @pSchoolId"
                sql2 = sql2.replace('@pSchoolId', str(si_details[0]['schoolId']))
                max_version = self.cu_obj.run_select_query_reporting_db(sql2)

                sql2 = "Insert Into dim_organizational_unit (countryId, countryName, provinceId, provinceName, " \
                       "districtId, districtName, schoolId, schoolName, schoolClientId, schoolDemo, version, " \
                       "etlUserName, etlDate) Values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                value2 = (si_details[0]['countryId'], si_details[0]['countryName'], si_details[0]['provinceId'],
                          si_details[0]['provinceName'], si_details[0]['districtId'], si_details[0]['districtName'],
                          si_details[0]['schoolId'], si_details[0]['schoolName'], si_details[0]['schoolClientId'],
                          si_details[0]['schoolDemo'], (max_version[0]['maxVersion'] + 1), self.REPORTING_DB_USER_NAME,
                          self.now_date_time.strftime("%Y-%m-%d"))

                self.cu_obj.run_single_insert_query_reporting_db(sql2, value2)
                dim_org_id = self.cu_obj.run_select_query_reporting_db(sql1)

            dim_org_unit_id = dim_org_id[0]['id']
        except Exception as e:
            print('An error occurred: ', e, ' -> ', traceback.format_exc())
        finally:
            return dim_org_unit_id

    def add_dim_survey_instance(self, si_details, dim_org_id):
        flag = False
        try:
            dim_target_group = self.get_dim_target_group(si_details['targetGroupId'])
            dim_brand = self.get_dim_brand(si_details['brandId'])

            sql = "Insert Into dim_survey_instance(surveyInstanceId, orgId, dimOrgUnitId, surveyDefinitionId, " \
                  "dimTargetGroupId, dimBrandId, brandPropertiesKey, yearDefId, snapShotId, surveyStartDate, " \
                  "surveyEndDate, answerParticipantCount, " \
                  "participantCharacteristicCount, grpGenderId, grpGradeId, grpSESId, grpSESHPId, grpImmigrantId, " \
                  "grpAboriginalId, grpAgeId, grpLangAtHomeId, grpGradeRepetitionId, grpEthnicityId, grpChgSchoolId, " \
                  "grpSpeEduId, grpFreImmId, grpDisabilityId, surveyClosedFlag, reportScheduled, reportGenerated, " \
                  "reportReleaseFlag, etlCompleteFlag, etlUserName, etlDate) " \
                  "Values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " \
                  "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            value = (si_details['id'], si_details['schoolId'], dim_org_id, si_details['surveyDefinitionId'],
                     dim_target_group[0]['id'], dim_brand[0]['id'], dim_brand[0]['brandPropertiesKey'],
                     si_details['yearDefId'], si_details['snapshotId'], si_details['startDate'], si_details['endDate'],
                     si_details['answerParticipantCount'], si_details['participantCharacteristicCount'],
                     1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, si_details['surveyClosedFlag'],
                     si_details['reportScheduled'], si_details['reportGenerated'], si_details['reportReleaseFlag'],
                     0, self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))

            self.cu_obj.run_single_insert_query_reporting_db(sql, value)
        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
            flag = False
        finally:
            return flag

    def add_si_measure(self, lst_dim_measure, dim_si_id):
        try:
            sql = "Insert Into survey_instance_measure(dimSurveyInstanceId, dimMeasureId, measureTypeId, " \
                  "etlUserName, etlDate) Values (%s, %s, %s, %s, %s)"

            lst_ins = list()
            for dim_measure in lst_dim_measure:
                tup = (str(dim_si_id), dim_measure['id'], dim_measure['measureTypeId'], self.REPORTING_DB_USER_NAME,
                       self.now_date_time.strftime("%Y-%m-%d"))
                lst_ins.append(tup)

            self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())

    def add_si_measure_question(self, dim_si_id):
        try:
            sql = "Select DISTINCT mq.id as dimMeasureQuestionId, mq.measureId, mq.questionId " \
                  "From measure_question as mq " \
                  "Inner Join survey_instance_measure as sim On sim.dimMeasureId = mq.dimMeasureId " \
                  "Order By mq.dimMeasureId, mq.dimQuestionId"
            lst_mq = self.cu_obj.run_select_query_reporting_db(sql)

            lst_ins = list()
            for mq in lst_mq:
                tup = (dim_si_id, mq['dimMeasureQuestionId'], self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                lst_ins.append(tup)

            sql = "Insert Into survey_instance_measure_question(dimSurveyInstanceId, dimMeasureQuestionId, " \
                  "etlUserName, etlDate) Values (%s, %s, %s, %s)"
            self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())

    def add_multiple_choice_answer(self, survey_instance_id, dim_si_id, lst_dim_measure):
        lst_dim_qn = None
        try:
            sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSurveyMCQDetails)
            sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
            lst_mcaq = self.cu_obj.run_select_query_survey_db(sql)

            if not ceu.CheckEmptyUtils.is_empty(lst_mcaq):
                str_qn_id = ""
                for mcaq in lst_mcaq:
                    if str(mcaq['questionId']) not in str_qn_id:
                        if str_qn_id == "":
                            str_qn_id = str(mcaq['questionId'])
                        else:
                            str_qn_id = str_qn_id + fc.FileConstants.COMMA_DELIMITER + str(mcaq['questionId'])

                sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimQuestionByQuestionId)
                sql = sql.replace('@pQuestionIds', str_qn_id)
                lst_dim_qn = self.cu_obj.run_select_query_reporting_db(sql)

                dict_qn = dict()
                for dim_qn in lst_dim_qn:
                    dict_qn[dim_qn['questionId']] = dim_qn['dimQuestionId']

                dict_measure = dict()
                for dim_measure in lst_dim_measure:
                    dict_measure[dim_measure['measureId']] = dim_measure['id']

                lst_ins = list()
                for mcaq in lst_mcaq:
                    tup = (dim_si_id, dict_measure[mcaq['measureId']], dict_qn[mcaq['questionId']], mcaq['answerDetailId'],
                           mcaq['questionTitle'], mcaq['questionText'], mcaq['label'], mcaq['description'], mcaq['ordering'],
                           self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                    lst_ins.append(tup)

                sql = "Insert Into drc_mcq (dimSurveyInstanceId, dimMeasureId, dimQuestionId, surveyMCQAnswerDetailId, " \
                      "questionTitle, questionText, shortLabel, fullLabel, ordering, etlUserName, etlDate) " \
                      "Values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return lst_dim_qn

    def add_bridge_mcq(self, survey_instance_id, dim_si_id, lst_dim_qn):
        dict_dim_qn_id_type = dict()
        for dim_qn in lst_dim_qn:
            dict_dim_qn_id_type[dim_qn['dimQuestionId']] = dim_qn['questionTypeId']
        lst_drc_mcq = list()
        try:
            sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDrcMcq)
            sql = sql.replace('@pDimSurveyInstanceId', str(dim_si_id))
            lst_drc_mcq = self.cu_obj.run_select_query_reporting_db(sql)

            lst_drc_mcq_id = list()
            dict_drc_mcq_id = dict()
            for drc_mcq in lst_drc_mcq:
                if drc_mcq['id'] not in lst_drc_mcq_id:
                    lst_drc_mcq_id.append(drc_mcq['id'])

                if drc_mcq['dimQuestionId'] in dict_drc_mcq_id:
                    mul_ans = dict_drc_mcq_id[drc_mcq['dimQuestionId']]
                    if drc_mcq['id'] not in mul_ans:
                        mul_ans.append(drc_mcq['id'])
                    dict_drc_mcq_id[drc_mcq['dimQuestionId']] = mul_ans
                else:
                    dict_drc_mcq_id[drc_mcq['dimQuestionId']] = [drc_mcq['id']]
                    lst_drc_mcq_id.append(str(drc_mcq['dimQuestionId']))

            dict_resp_by_qn_id = dict()
            for qn_id, lst_resp in dict_drc_mcq_id.items():
                tmp_lst_all_resp = list()
                num_of_res = len(lst_resp)
                cnt = 1

                while cnt <= num_of_res:
                    comb = combinations(lst_resp, cnt)
                    cnt = cnt + 1
                    tmp_lst_all_resp.append(comb)

                    if dict_dim_qn_id_type[qn_id] == 6:
                        break

                lst_all_resp = list()
                lst_all_resp.append(["NULL" + str(qn_id)])
                for lst in tmp_lst_all_resp:
                    for tup in lst:
                        lst_all_resp.append(list(tup))

                dict_resp_by_qn_id[qn_id] = lst_all_resp

            lst_qn_id = list()
            lst_final = list()
            for qn_id1, lst_resp1 in dict_resp_by_qn_id.items():
                for qn_id2, lst_resp2 in dict_resp_by_qn_id.items():
                    if qn_id1 == qn_id2:
                        continue
                    else:
                        if len(lst_qn_id) == 0:
                            if len(lst_final) == 0:
                                lst_mul = self.multiply_lists(lst_resp1, lst_resp2)
                            else:
                                lst_mul = self.multiply_lists(lst_final, lst_resp2)
                            lst_final = lst_mul

                        elif len(lst_qn_id) > 0:
                            for element in lst_qn_id:
                                if qn_id1 in element and qn_id2 in element:
                                    continue
                                else:
                                    if len(lst_final) == 0:
                                        lst_mul = self.multiply_lists(lst_resp1, lst_resp2)
                                    else:
                                        lst_mul = self.multiply_lists(lst_final, lst_resp2)
                                    lst_final = lst_mul
                        lst_qn_id.append((qn_id1, qn_id2))

            sql1 = "Insert Into bridge_mcq (dimSurveyInstanceId, groupId, drcMcqId, etlUserName, etlDate) " \
                   "Values(%s, %s, %s, %s, %s)"
            lst_ins = list()
            sql2 = "Select IF(MAX(groupId) IS NULL, 0, MAX(groupId)) as maxGroupId From bridge_mcq"

            max_group_id = self.cu_obj.run_select_query_reporting_db(sql2)
            max_group_id = max_group_id[0]['maxGroupId']
            max_group_id = max_group_id + 1

            for lst in lst_final:
                for element in lst:
                    if "NULL" in str(element):
                        element = None
                    tup = (dim_si_id, max_group_id, element, self.REPORTING_DB_USER_NAME,
                           self.now_date_time.strftime("%Y-%m-%d"))
                    lst_ins.append(tup)
                max_group_id = max_group_id + 1
            self.cu_obj.run_multiple_insert_query_reporting_db(sql1, lst_ins)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return lst_drc_mcq

    @staticmethod
    def multiply_lists(lst_resp1, lst_resp2):
        lst_ret = list()
        for lst1 in lst_resp1:
            for lst2 in lst_resp2:
                lst_new = list()
                lst_new.extend(lst1)
                lst_new.extend(lst2)
                lst_ret.append(lst_new)

        return lst_ret
