import json
import controller.CommonStoreController as csc


class EtlRepStoreParticipantMeasureScoreController(csc.CommonStoreController):
    # gets a message from etlRepSqsParticipantMeasureScore and inserts them into fact_student_measure_score
    def entry_function(self, ps):
        if ps is None:
            ps = self.sqs_obj.retrieve_message('etlRepSqsParticipantMeasureScore', True)

        if ps is not None:
            ps = json.loads(ps)
            for key, value in ps.items():
                survey_participant_id = key
            print('survey_participant_id -> ', survey_participant_id)
            ps = ps[survey_participant_id]
            dict_dim_sp = self.get_dim_survey_participant(ps)  # have to include completed field in dim_survey_participant

            pms = ps['pms']
            if pms is not None:
                sql = "Insert Into fact_student_measure_score(" \
                      "dimOrgUnitId, " \
                      "dimSurveyInstanceId, " \
                      "dimSurveyParticipantId, " \
                      "dimMeasureId, " \
                      "dimCategoryId, " \
                      "scaledScore, " \
                      "percentScore, " \
                      "percentScoreMid, " \
                      "percentScoreHigh, " \
                      "replicaScaledScore, " \
                      "replicaPercentScore, " \
                      "replicaPercentScoreMid, " \
                      "replicaPercentScoreHigh, " \
                      "surveyDate, " \
                      "etlUserName, " \
                      "etlDate) " \
                      "Values (" \
                      "%s, %s, %s, %s, %s, " \
                      "%s, %s, %s, %s, %s, %s, " \
                      "%s, %s, %s, %s, %s)"

                lst_ins = list()
                tup_fact = tuple()
                for dim_measure_id, score in pms['dimMeasureScore'].items():
                    tup_fact = (pms['dimOrgUnitId'], pms['dimSurveyInstanceId'], dict_dim_sp['dimSurveyParticipantId'],
                                int(dim_measure_id), 1, score['scaledScore'], score['percentScore'], score['percentScoreMid'],
                                score['percentScoreHigh'], score['replicaScaledScore'], score['replicaPercentScore'],
                                score['replicaPercentScoreMid'], score['replicaPercentScoreHigh'], pms['surveyDate'],
                                self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                    lst_ins.append(tup_fact)

                if len(lst_ins) > 1:
                    self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                else:
                    self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

            pqs = ps['pqs']
            if pqs is not None:
                # insertion of likert questions
                sql = "Insert Into fact_student_question_likert_score(" \
                      "dimOrgUnitId, " \
                      "dimSurveyInstanceId, " \
                      "dimSurveyParticipantId, " \
                      "dimMeasureId, " \
                      "dimQuestionId, " \
                      "dimLikertAnswerDetailId, " \
                      "scaledScore, " \
                      "surveyDate, " \
                      "etlUserName, " \
                      "etlDate) " \
                      "Values (" \
                      "%s, %s, %s, %s, %s, " \
                      "%s, %s, %s, %s, %s)"

                lst_ins = list()
                tup_fact = tuple()
                for dim_measure_id, dim_ql in pqs['dimAnswerLikert'].items():
                    for dim_question_id, dim_lad_id in dim_ql['answer'].items():
                        score = None  # score has been made none. have to ensure its not the case

                        tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'], dict_dim_sp['dimSurveyParticipantId'],
                                    dim_measure_id, dim_question_id, dim_lad_id, score,  pqs['surveyDate'],
                                    self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                        lst_ins.append(tup_fact)

                if len(lst_ins) > 1:
                    self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                else:
                    self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

                # insertion of oeq questions
                if pqs['dimAnswerOEQ'] is not None:
                    sql = "Insert Into fact_student_oeq (" \
                          "dimOrgUnitId, " \
                          "dimSurveyInstanceId, " \
                          "dimSurveyParticipantId, " \
                          "dimMeasureId, " \
                          "dimQuestionId, " \
                          "answer, " \
                          "surveyDate, " \
                          "etlUserName, " \
                          "etlDate) " \
                          "Values " \
                          "(%s, %s, %s, %s, %s," \
                          "%s, %s, %s, %s)"

                    lst_ins = list()
                    tup_fact = tuple()
                    for dim_measure_id, dim_qn in pqs['dimAnswerOEQ'].items():
                        for dim_question_id, answer in dim_qn['answer'].items():

                            tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                        dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id, answer,  pqs['surveyDate'],
                                        self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                            lst_ins.append(tup_fact)

                    if len(lst_ins) > 1:
                        self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                    else:
                        self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

                # insertion of mcq maq questions
                if pqs['dimAnswerMCAQ'] is not None:
                    sql = "Insert Into fact_student_mcq (" \
                          "dimOrgUnitId, " \
                          "dimSurveyInstanceId, " \
                          "dimSurveyParticipantId, " \
                          "dimMeasureId, " \
                          "dimQuestionId, " \
                          "dimMCQAnswerDetailId, " \
                          "surveyDate, " \
                          "etlUserName, " \
                          "etlDate) " \
                          "Values " \
                          "(%s, %s, %s, %s, %s," \
                          "%s, %s, %s, %s)"

                    lst_ins = list()
                    tup_fact = tuple()

                    for dim_measure_id, dim_qn in pqs['dimAnswerMCAQ'].items():
                        for dim_question_id, dim_mcq_answer_detail_id in dim_qn['answer'].items():
                            if isinstance(dim_mcq_answer_detail_id, int):
                                tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                            dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id,
                                            dim_mcq_answer_detail_id,  pqs['surveyDate'], self.REPORTING_DB_USER_NAME,
                                            self.now_date_time.strftime("%Y-%m-%d"))
                                lst_ins.append(tup_fact)

                            elif isinstance(dim_mcq_answer_detail_id, list):
                                for ans_id in dim_mcq_answer_detail_id:
                                    tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                                dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id,
                                                ans_id, pqs['surveyDate'], self.REPORTING_DB_USER_NAME,
                                                self.now_date_time.strftime("%Y-%m-%d"))
                                    lst_ins.append(tup_fact)

                    if len(lst_ins) > 1:
                        self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                    else:
                        self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)
