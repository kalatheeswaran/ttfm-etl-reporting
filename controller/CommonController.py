import datetime
import utils.SQSUtils as sqsUtils
import utils.ConnectionUtils as cu
import utils.CheckEmptyUtils as emptyUtils
from abc import ABC, abstractmethod
import traceback


class CommonController(ABC):
    REPORTING_DB_USER_NAME = 'osreportingsys'
    now_date_time = datetime.datetime.now()
    cu_obj = cu.ConnectionUtils()
    sqs_obj = sqsUtils.SQSUtils()
    empty_utils_obj = emptyUtils.CheckEmptyUtils()

    @staticmethod
    def read_sql(sql_file_name):
        with open(sql_file_name, 'r') as sqlFile:
            sql = sqlFile.read()
            return sql

    @staticmethod
    def print_sql_result(sql_result):
        for row in sql_result:
            print(row[0])

    @abstractmethod
    def entry_function(self):
        pass

    def get_dim_measures(self, measure_ids):
        lst_dim_measure = list()
        try:
            sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimMeasureFromMeasureId)
            sql = sql.replace('@pMeasureIds', str(measure_ids))
            lst_dim_measure = self.cu_obj.run_select_query_reporting_db(sql)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return lst_dim_measure

    def get_dim_survey_instance(self, survey_instance_id):
        dim_si = None
        try:
            sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimSurveyInstance)
            sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
            dim_si = self.cu_obj.run_select_query_reporting_db(sql)

        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
        finally:
            return dim_si
