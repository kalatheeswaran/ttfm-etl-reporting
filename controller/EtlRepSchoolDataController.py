import controller.CommonController as cc
import constants.SQLConstants as sc
import traceback


class EtlRepSchoolDataController(cc.CommonController, sc.SQLConstants):
    def entry_function(self):
        print('In entry function of EtlRepSchoolDataController...')
        survey_instance_id = 113541
        dim_si_id = 546
        dim_si = self.get_dim_survey_instance(survey_instance_id)

        flag = self.delete_dim_survey_instance_entries(dim_si_id)
        if flag is True:
            # self.transform_student_measure_score(dim_si_id, dim_si)
            self.transform_student_question_likert(dim_si_id, dim_si)

    def delete_dim_survey_instance_entries(self, dim_survey_instance_id):
        flag = False
        try:
            sql = 'Delete From fact_school_measure_score Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_school_measure_score')

            sql = 'Delete From fact_school_question_likert Where dimSurveyInstanceId = %s'
            self.cu_obj.run_delete_query_reporting_db(sql, dim_survey_instance_id, 'fact_school_question_likert')

            flag = True
        except Exception as e:
            print('An error occurred : ', e, ' -> ', traceback.format_exc())
            flag = False
        finally:
            return flag

    def transform_student_measure_score(self, dim_si_id, dim_si):
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetFactStudentMeasureScore)
        sql = sql.replace('@pDimSurveyInstanceId', str(dim_si_id))
        sql_result = self.cu_obj.run_select_query_reporting_db(sql)

        dict_school_measure = dict()
        dict_sp_measure = dict()
        dim_measure_id = None
        sql_result_len = len(sql_result)
        cnt = 0

        for i in sql_result:
            tmp_dim_measure_id = i['dimMeasureId']
            cnt = cnt + 1

            if tmp_dim_measure_id == dim_measure_id or dim_measure_id is None:
                dict_sp_measure = self.transform_measure_func(i, dict_sp_measure)

                if cnt == sql_result_len:
                    dict_school_measure[dim_measure_id] = dict_sp_measure

            else:
                dict_school_measure[dim_measure_id] = dict_sp_measure
                dict_sp_measure = dict()
                dict_sp_measure = self.transform_measure_func(i, dict_sp_measure)

            dim_measure_id = tmp_dim_measure_id

        self.insert_into_fact_school_measure_score(dim_si, dict_school_measure)

    @staticmethod
    def transform_measure_func(i, dict_sp_measure):
        tup = (i['dimGenderId'], i['dimGradeId'])
        if tup in dict_sp_measure.keys():
            if i['scaledScore'] in dict_sp_measure[tup].keys():
                count = dict_sp_measure[tup][i['scaledScore']]['count']
                count = count + 1

                tmp_dict = dict_sp_measure[tup]
                tmp_dict[i['scaledScore']] = {
                    'percentScore': i['percentScore'],
                    'count': count
                }
                dict_sp_measure[tup] = tmp_dict
            else:
                tmp_dict = dict_sp_measure[tup]
                tmp_dict[i['scaledScore']] = {
                    'percentScore': i['percentScore'],
                    'count': 1
                }
                dict_sp_measure[tup] = tmp_dict
        else:
            dict_sp_measure[tup] = {
                i['scaledScore']: {
                    'percentScore': i['percentScore'],
                    'count': 1
                }
            }
        return dict_sp_measure

    def insert_into_fact_school_measure_score(self, dim_si, dict_school_measure):
        dim_si_id = dim_si[0]['id']
        dim_org_unit_id = dim_si[0]['dimOrgUnitId']
        dim_target_group_id = dim_si[0]['dimTargetGroupId']

        sql = 'Insert Into fact_school_measure_score' \
              '(dimOrgUnitId, ' \
              'dimTargetGroupId, ' \
              'dimSurveyInstanceId, ' \
              'dimMeasureId, ' \
              'dimGenderId, ' \
              'dimGradeId, ' \
              'scaledScore, ' \
              'percentScore, ' \
              'count, ' \
              'etlUserName, ' \
              'etlDate) ' \
              'Values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'

        lst_ins = list()
        tup_fact = tuple()
        for dim_measure_id, dict_sp_measure in dict_school_measure.items():
            for tup, val in dict_sp_measure.items():
                dim_gender_id = tup[0]
                dim_grade_id = tup[1]
                for score, entries in val.items():
                    tup_fact = (dim_org_unit_id, dim_target_group_id, dim_si_id, dim_measure_id,
                                dim_gender_id, dim_grade_id, float(score),
                                entries['percentScore'], entries['count'],
                                self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                    lst_ins.append(tup_fact)

        if len(lst_ins) > 1:
            self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
        else:
            self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

    def transform_student_question_likert(self, dim_si_id, dim_si):
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetFactStudentQuestionLikert)
        sql = sql.replace('@pDimSurveyInstanceId', str(dim_si_id))
        sql_result = self.cu_obj.run_select_query_reporting_db(sql)

        dict_school_measure = dict()
        dict_school_qn = dict()
        dict_sp_qn_likert = dict()
        dim_measure_id = None
        dim_qn_id = None
        sql_result_len = len(sql_result)
        cnt = 0

        for i in sql_result:
            tmp_dim_measure_id = i['dimMeasureId']
            tmp_dim_qn_id = i['dimQuestionId']
            cnt = cnt + 1

            if tmp_dim_measure_id == dim_measure_id or dim_measure_id is None:
                if tmp_dim_qn_id == dim_qn_id or dim_qn_id is None:
                    dict_sp_qn_likert = self.transform_qn_func(i, dict_sp_qn_likert)

                    if cnt == sql_result_len:
                        dict_school_qn[dim_qn_id] = dict_sp_qn_likert
                        dict_school_measure[dim_measure_id] = dict_school_qn
                else:
                    dict_school_qn[dim_qn_id] = dict_sp_qn_likert
                    dict_school_measure[dim_measure_id] = dict_school_qn

                    dict_sp_qn_likert = dict()
                    dict_sp_qn_likert = self.transform_qn_func(i, dict_sp_qn_likert)

            else:
                dict_school_qn[dim_qn_id] = dict_sp_qn_likert
                dict_school_measure[dim_measure_id] = dict_school_qn

                dict_school_qn = dict()
                dict_sp_qn_likert = dict()

            dim_measure_id = tmp_dim_measure_id
            dim_qn_id = tmp_dim_qn_id

        self.insert_into_fact_school_question_likert(dim_si, dict_school_measure)

    @staticmethod
    def transform_qn_func(i, dict_sp_qn_likert):
        tup = (i['dimGenderId'], i['dimGradeId'])
        if tup in dict_sp_qn_likert.keys():
            if i['dimLikertAnswerDetailId'] in dict_sp_qn_likert[tup].keys():
                count = dict_sp_qn_likert[tup][i['dimLikertAnswerDetailId']]['count']
                count = count + 1

                tmp_dict = dict_sp_qn_likert[tup]
                tmp_dict[i['dimLikertAnswerDetailId']] = {
                    'count': count
                }
                dict_sp_qn_likert[tup] = tmp_dict
            else:
                tmp_dict = dict_sp_qn_likert[tup]
                tmp_dict[i['dimLikertAnswerDetailId']] = {
                    'count': 1
                }
                dict_sp_qn_likert[tup] = tmp_dict
        else:
            dict_sp_qn_likert[tup] = {
                i['dimLikertAnswerDetailId']: {
                    'count': 1
                }
            }
        return dict_sp_qn_likert

    def insert_into_fact_school_question_likert(self, dim_si, dict_school_measure):
        dim_si_id = dim_si[0]['id']
        dim_org_unit_id = dim_si[0]['dimOrgUnitId']
        dim_target_group_id = dim_si[0]['dimTargetGroupId']

        sql = 'Insert Into fact_school_question_likert' \
              '(dimOrgUnitId, ' \
              'dimTargetGroupId, ' \
              'dimSurveyInstanceId, ' \
              'dimMeasureId, ' \
              'dimQuestionId, ' \
              'dimGenderId, ' \
              'dimGradeId, ' \
              'dimLikertAnswerDetailId, ' \
              'scaledScore, ' \
              'total, ' \
              'etlUserName, ' \
              'etlDate) ' \
              'Values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'

        lst_ins = list()
        tup_fact = tuple()

        for dim_measure_id, dict_qn in dict_school_measure.items():
            for dim_qn_id, dict_tup in dict_qn.items():
                for tup_dim, dict_lad in dict_tup.items():
                    for dim_lad_id, val in dict_lad.items():
                        tup_fact = (dim_org_unit_id, dim_target_group_id, dim_si_id, dim_measure_id,
                                    dim_qn_id, tup_dim[0], tup_dim[1], dim_lad_id, 0,
                                    val['count'], self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                        lst_ins.append(tup_fact)

        if len(lst_ins) > 1:
            self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
        else:
            self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)


