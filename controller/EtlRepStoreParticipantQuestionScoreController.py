import json
import controller.CommonStoreController as csc


class EtlRepStoreParticipantQuestionScoreController(csc.CommonStoreController):
    # gets a message from etlRepSqsParticipantQuestionScore and inserts them into fact_student_question_likert_score
    def entry_function(self, pqs):
        if pqs is None:
            pqs = self.sqs_obj.retrieve_message('etlRepSqsParticipantQuestionScore', False)

        if pqs is not None:
            pqs = json.loads(pqs)
            for key, value in pqs.items():
                survey_participant_id = key

            pqs = pqs[survey_participant_id]
            dict_dim_sp = self.get_dim_survey_participant(pqs)

            # insertion of likert questions
            sql = "Insert Into fact_student_question_likert_score(" \
                  "dimOrgUnitId, " \
                  "dimSurveyInstanceId, " \
                  "dimSurveyParticipantId, " \
                  "dimMeasureId, " \
                  "dimQuestionId, " \
                  "dimLikertAnswerDetailId, " \
                  "scaledScore, " \
                  "surveyDate, " \
                  "etlUserName, " \
                  "etlDate) " \
                  "Values (" \
                  "%s, %s, %s, %s, %s, " \
                  "%s, %s, %s, %s, %s)"

            lst_ins = list()
            tup_fact = tuple()
            for dim_measure_id, dim_ql in pqs['pqs']['dimAnswerLikert'].items():
                for dim_question_id, dim_lad_id in dim_ql['answer'].items():
                    score = None  # score has been made none. have to ensure its not the case

                    tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'], dict_dim_sp['dimSurveyParticipantId'],
                                dim_measure_id, dim_question_id, dim_lad_id, score,  pqs['surveyDate'],
                                self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                    lst_ins.append(tup_fact)

            if len(lst_ins) > 1:
                self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
            else:
                self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

            # insertion of oeq questions
            if pqs['dimAnswerOEQ'] is not None:
                sql = "Insert Into fact_student_oeq (" \
                      "dimOrgUnitId, " \
                      "dimSurveyInstanceId, " \
                      "dimSurveyParticipantId, " \
                      "dimMeasureId, " \
                      "dimQuestionId, " \
                      "answer, " \
                      "surveyDate, " \
                      "etlUserName, " \
                      "etlDate) " \
                      "Values " \
                      "(%s, %s, %s, %s, %s," \
                      "%s, %s, %s, %s)"

                lst_ins = list()
                tup_fact = tuple()
                for dim_measure_id, dim_qn in pqs['pqs']['dimAnswerOEQ'].items():
                    for dim_question_id, answer in dim_qn['answer'].items():

                        tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                    dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id, answer,  pqs['surveyDate'],
                                    self.REPORTING_DB_USER_NAME, self.now_date_time.strftime("%Y-%m-%d"))
                        lst_ins.append(tup_fact)

                if len(lst_ins) > 1:
                    self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                else:
                    self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)

            # insertion of mcq maq questions
            if pqs['dimAnswerMCAQ'] is not None:
                sql = "Insert Into fact_student_mcq (" \
                      "dimOrgUnitId, " \
                      "dimSurveyInstanceId, " \
                      "dimSurveyParticipantId, " \
                      "dimMeasureId, " \
                      "dimQuestionId, " \
                      "dimMCQAnswerDetailId, " \
                      "surveyDate, " \
                      "etlUserName, " \
                      "etlDate) " \
                      "Values " \
                      "(%s, %s, %s, %s, %s," \
                      "%s, %s, %s, %s)"

                lst_ins = list()
                tup_fact = tuple()

                for dim_measure_id, dim_qn in pqs['pqs']['dimAnswerMCAQ'].items():
                    for dim_question_id, dim_mcq_answer_detail_id in dim_qn['answer'].items():
                        if isinstance(dim_mcq_answer_detail_id, int):
                            tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                        dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id,
                                        dim_mcq_answer_detail_id,  pqs['surveyDate'], self.REPORTING_DB_USER_NAME,
                                        self.now_date_time.strftime("%Y-%m-%d"))
                            lst_ins.append(tup_fact)

                        elif isinstance(dim_mcq_answer_detail_id, list):
                            for ans_id in dim_mcq_answer_detail_id:
                                tup_fact = (pqs['dimOrgUnitId'], pqs['dimSurveyInstanceId'],
                                            dict_dim_sp['dimSurveyParticipantId'], dim_measure_id, dim_question_id,
                                            ans_id, pqs['surveyDate'], self.REPORTING_DB_USER_NAME,
                                            self.now_date_time.strftime("%Y-%m-%d"))
                                lst_ins.append(tup_fact)

                if len(lst_ins) > 1:
                    self.cu_obj.run_multiple_insert_query_reporting_db(sql, lst_ins)
                else:
                    self.cu_obj.run_single_insert_query_reporting_db(sql, tup_fact)
