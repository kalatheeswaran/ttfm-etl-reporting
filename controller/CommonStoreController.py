import controller.CommonController as cc
from abc import ABC, abstractmethod
import constants.SQLConstants as fc
import json


class CommonStoreController(cc.CommonController, fc.SQLConstants, ABC):
    @abstractmethod
    def entry_function(self):
        pass

    # function which checks if an entry exists for a surveyParticipantId in the dim_survey_participant table
    # and returns the id in a dictionary. If not inserts the participant into the table and returns the inserted data
    def get_dim_survey_participant(self, dict_ps):
        dict_ps = dict_ps['pqs']
        sql1 = "Select id From dim_survey_participant Where surveyParticipantId = @pSurveyParticipantId"
        sql1 = sql1.replace('@pSurveyParticipantId', str(dict_ps['surveyParticipantId']))
        sql_result1 = self.cu_obj.run_select_query_reporting_db(sql1)

        if self.empty_utils_obj.is_empty(sql_result1):
            sql2 = "Insert Into dim_survey_participant (" \
                   "surveyParticipantId, " \
                   "dimSurveyInstanceId, " \
                   "dimOrgUnitId, " \
                   "cellNum, " \
                   "personalId, " \
                   "dimGenderId, " \
                   "dimGradeId, " \
                   "dimSESId, " \
                   "dimSESHPId, " \
                   "dimImmigrantId, " \
                   "dimAboriginalId, " \
                   "dimAgeId, " \
                   "dimCustMeasureId, " \
                   "dimLangAtHomeId, " \
                   "dimGradeRepetitionId, " \
                   "dimEthnicityId, " \
                   "dimChgSchoolId, " \
                   "dimSpeEduId, " \
                   "dimFreImmId, " \
                   "dimDisabilityId, " \
                   "surveyStartDate, " \
                   "surveyEndDate, " \
                   "etlUserName, " \
                   "etlDate) " \
                   "Values (" \
                   "%s, %s, %s, %s, %s, " \
                   "%s, %s, %s, %s, %s, " \
                   "%s, %s, %s, %s, %s, " \
                   "%s, %s, %s, %s, %s," \
                   "%s, %s, %s, %s)"

            value2 = (dict_ps['surveyParticipantId'],
                      dict_ps['dimSurveyInstanceId'],
                      dict_ps['dimOrgUnitId'],
                      dict_ps['dimension']['cellNum'],

                      None,
                      dict_ps['dimension']['dimGenderId'],
                      dict_ps['dimension']['dimGradeId'],
                      dict_ps['dimension']['dimSESId'],

                      dict_ps['dimension']['dimSESHPId'],
                      dict_ps['dimension']['dimImmigrantId'],
                      dict_ps['dimension']['dimAboriginalId'],
                      dict_ps['dimension']['dimAgeId'],

                      dict_ps['dimension']['dimCustMeasureId'],
                      dict_ps['dimension']['dimLangAtHomeId'],
                      dict_ps['dimension']['dimGradeRepetitionId'],
                      dict_ps['dimension']['dimEthnicityId'],

                      dict_ps['dimension']['dimChangeSchoolId'],
                      dict_ps['dimension']['dimSpecialEducationId'],
                      dict_ps['dimension']['dimFrenchImmersionId'],
                      dict_ps['dimension']['dimDisabilityId'],

                      None, None,
                      self.REPORTING_DB_USER_NAME,
                      self.now_date_time.strftime("%Y-%m-%d"))

            self.cu_obj.run_single_insert_query_reporting_db(sql2, value2)
            sql_result1 = self.cu_obj.run_select_query_reporting_db(sql1)

            dict_ps['dimSurveyParticipantId'] = sql_result1[0]['id']

        else:
            dict_ps['dimSurveyParticipantId'] = sql_result1[0]['id']

        return dict_ps
