import json
import controller.CommonController as cc
import constants.SQLConstants as sc
import constants.FileConstants as fc
import constants.DimTargetGroupConstants as dimTG
from datetime import datetime


class EtlRepParticipantDataController(cc.CommonController, sc.SQLConstants):
    # function to retrieve and delete message from etRepSqsSurveyInstance queue. Calls the get_participant_data()
    # and get_survey_instance_metadata() and sends the messages to etlRepSqsParticipantData
    def entry_function(self, si):
        if si is None:
            si = self.sqs_obj.retrieve_message('etlRepSqsSurveyInstance', True)

        if si is not None:
            si = json.loads(si)
            if 'surveyInstanceId' in si:
                now1 = datetime.now()

                survey_instance_id = str(si['surveyInstanceId'])
                dim_si_metadata = self.get_dim_survey_instance_metadata(survey_instance_id)

                dim_si_metadata = json.loads(dim_si_metadata)
                lst_sp = self.get_participant_data(dim_si_metadata)

                now2 = datetime.now()
                print("Total time => ", now2 - now1)

                now1 = datetime.now()
                cnt = 0
                j = 0
                k = 0
                lst_pms = list()
                len_lst_sp = len(lst_sp)

                for i in lst_sp:
                    k = k + 1
                    cnt = cnt + 1
                    for key, value in i.items():
                        j = j + 1
                        if value['dimPMS'] is not None:
                            dict_pms = dict()
                            dict_pms[key] = {
                                'pms': value['dimPMS'],
                                'pqs': value['dimAnswer']
                            }

                            tmp_dict_pms = {
                                'Id': str(j),
                                'MessageBody': json.dumps(dict_pms)
                            }
                            lst_pms.append(tmp_dict_pms)

                        if cnt == 2 or (cnt < 2 and k == len_lst_sp):
                            res1 = self.sqs_obj.send_message_in_batch('etlRepSqsParticipantMeasureScore', lst_pms)
                            cnt = 0
                            lst_pms = list()

                now2 = datetime.now()
                print("Total time => ", now2 - now1)
            else:
                print('No valid survey instance')

    def get_sub_dim_measures_list(self, survey_instance_id, dim_si_measures, lst_dim_si_metadata):
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimSurveyInstanceMetadataForSubMeasures)
        sql = sql.replace('@pSurveyInstanceId', survey_instance_id)
        sql = sql.replace('@pDimMeasureIds', dim_si_measures)
        lst_tmp_dim_si_metadata = self.cu_obj.run_select_query_reporting_db(sql)

        if len(lst_tmp_dim_si_metadata) != 0:
            lst_dim_si_metadata.extend(lst_tmp_dim_si_metadata)

        if lst_tmp_dim_si_metadata is not None:
            dim_si_measures = self.get_dim_si_measures(lst_tmp_dim_si_metadata)
            if dim_si_measures is not None:
                self.get_sub_dim_measures_list(survey_instance_id, dim_si_measures, lst_dim_si_metadata)

        return lst_dim_si_metadata

    @staticmethod
    def get_dim_si_measures(lst_dim_si_metadata):
        dim_si_measures = None
        for dim_si_metadata in lst_dim_si_metadata:
            if dim_si_measures is None:
                dim_si_measures = str(dim_si_metadata['dimMeasureId'])
            else:
                dim_si_measures = dim_si_measures + fc.FileConstants.COMMA_DELIMITER + str(dim_si_metadata['dimMeasureId'])
        return dim_si_measures

    def get_dict_measure_question(self, dim_si_measures):
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimMeasureFromDimMeasureId)
        sql = sql.replace('@pDimMeasureIds', dim_si_measures)
        lst_dim_measure = self.cu_obj.run_select_query_reporting_db(sql)

        dict_measure_qn = dict()
        measure_id = None
        for i in lst_dim_measure:
            tmp_measure_id = i['measureId']

            if measure_id != tmp_measure_id:
                measure_id = tmp_measure_id

                dict_qn = dict()
                for j in lst_dim_measure:
                    if measure_id == j['measureId']:
                        dict_qn[j['questionId']] = j['dimQuestionId']

                dict_measure_qn[measure_id] = {
                    measure_id: i['dimMeasureId'],
                    "measureQuestion": dict_qn
                }

        return dict_measure_qn

    # gets the measure details of the survey instance along with the measure question answer detail scores
    # from ana_mqladscore table in tlb and sends them in a json format
    def get_dim_survey_instance_metadata(self, survey_instance_id):
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDimSurveyInstanceMetadata)
        sql = sql.replace('@pSurveyInstanceId', survey_instance_id)
        lst_dim_si_metadata = self.cu_obj.run_select_query_reporting_db(sql)

        dim_si_measures = self.get_dim_si_measures(lst_dim_si_metadata)

        lst_tmp_dim_si_metadata = self.get_sub_dim_measures_list(survey_instance_id, dim_si_measures, [])
        lst_dim_si_metadata.extend(lst_tmp_dim_si_metadata)
        dim_si_measures = self.get_dim_si_measures(lst_dim_si_metadata)

        dict_measure_qn = self.get_dict_measure_question(dim_si_measures)

        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetMeasureQuestionScore)
        sql = sql.replace('@pDimMeasureIds', dim_si_measures)
        lst_mqs = self.cu_obj.run_select_query_reporting_db(sql)

        dict_mqs = self.get_dict_mqs(lst_mqs)
        dict_rep_mqs = self.get_dict_rep_mqs(lst_mqs)

        dict_dim_mqs = dict()
        lst_dim_measure_id = list()
        lst_measure_id = list()

        for i in lst_dim_si_metadata:
            if i['skipComputingMeasure'] == 0:
                question = None
                dim_question = None

                if i['measureTypeId'] == 1:
                    if i['measureId'] in dict_mqs:
                        question = dict_mqs[i['measureId']]['question']

                    if i['dimMeasureId'] in dict_rep_mqs:
                        dim_question = dict_rep_mqs[i['dimMeasureId']]['dimQuestion']

                measure = {
                    'measureId': i['measureId'],
                    'dimMeasureId': i['dimMeasureId'],
                    'measureName': i['measureName'],
                    'measureTypeId': i['measureTypeId'],
                    'defaultChartDataTypeId': i['defaultChartDataTypeId'],

                    'parentDimMeasureId': i['parentDimMeasureId'],
                    'minimumAnswersRequired': i['minimumAnswersRequired'],
                    'calcRuleName': i['calcRuleName'],
                    'aggregateRuleName': i['aggregateRuleName'],

                    'minVal': i['minVal'],
                    'maxVal': i['maxVal'],
                    'isMultiCutoff': i['isMultiCutoff'],
                    'cutoff': i['cutoff'],

                    'multiCutoff1': i['multiCutoff1'],
                    'multiCutoff2': i['multiCutoff2'],
                    'dispUnit': i['dispUnit'],
                    'question': question,
                    'dimQuestion': dim_question
                }

                dict_dim_mqs[i['dimMeasureId']] = measure
                lst_dim_measure_id.append(i['dimMeasureId'])
                lst_measure_id.append(i['measureId'])

        measure_ids = None
        for i in lst_measure_id:
            if measure_ids is None:
                measure_ids = str(i)
            else:
                measure_ids = measure_ids + fc.FileConstants.COMMA_DELIMITER + str(i)

        lst_dim_measure = self.get_dim_measures(measure_ids)
        dict_measure = dict()
        for dim_measure in lst_dim_measure:
            dict_measure[dim_measure['measureId']] = dim_measure['id']

        dim_si_metadata = {
            'surveyInstanceId': lst_dim_si_metadata[0]['surveyInstanceId'],
            'dimSurveyInstanceId': lst_dim_si_metadata[0]['dimSurveyInstanceId'],
            'orgUnitId': lst_dim_si_metadata[0]['orgUnitId'],
            'dimOrgUnitId': lst_dim_si_metadata[0]['dimOrgUnitId'],

            'surveyDefinitionId': lst_dim_si_metadata[0]['surveyDefinitionId'],
            'dimTargetGroupId': lst_dim_si_metadata[0]['dimTargetGroupId'],
            'measureIds': lst_measure_id,
            'dimMeasureIds': lst_dim_measure_id,

            'dictMeasures': dict_measure,
            'mqs': dict_mqs,
            'dimMQS': dict_dim_mqs,
            'measureQuestion': dict_measure_qn
        }

        dim_si_metadata = json.dumps(dim_si_metadata)
        return dim_si_metadata

    # get the answer data for a given survey instance for measureId 1 and calls the get_participant_info() function
    # and organizes the data into a json format and returns them in a list
    def get_participant_data(self, dim_si_metadata):
        survey_instance_id = dim_si_metadata['surveyInstanceId']
        dim_survey_instance_id = dim_si_metadata['dimSurveyInstanceId']
        org_id = dim_si_metadata['orgUnitId']
        dim_org_id = dim_si_metadata['dimOrgUnitId']
        dim_target_group_id = dim_si_metadata['dimTargetGroupId']

        dict_sp_rep = self.get_replica_score_data(dim_si_metadata)

        lst_measure_id = dim_si_metadata['measureIds']
        measure_ids = None
        for i in lst_measure_id:
            if measure_ids is None:
                measure_ids = str(i)
            else:
                measure_ids = measure_ids + fc.FileConstants.COMMA_DELIMITER + str(i)

        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSurveyParticipants)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        lst_survey_participant = self.cu_obj.run_select_query_survey_db(sql)

        dict_sp_likert_data = self.get_likert_answers(survey_instance_id, measure_ids, dim_si_metadata)
        dict_sp_oeq_data = self.get_open_ended_answers(survey_instance_id, measure_ids, dim_si_metadata)
        dict_sp_mcaq_data = self.get_mcaq_answers(survey_instance_id, measure_ids, dim_si_metadata)
        dict_pms = self.get_participant_measure_score(survey_instance_id, measure_ids, dict_sp_rep, dim_si_metadata)
        dict_sp_info = self.get_participant_info(survey_instance_id, dim_survey_instance_id, dim_target_group_id, dict_sp_mcaq_data)

        lst_sp = list()

        for i in lst_survey_participant:
            sp_id = i['surveyParticipantId']

            dim_answer_likert = None
            if sp_id in dict_sp_likert_data:
                dim_answer_likert = dict_sp_likert_data[sp_id]

            dim_answer_oeq = None
            if sp_id in dict_sp_oeq_data:
                dim_answer_oeq = dict_sp_oeq_data[sp_id]

            dim_answer_mcaq = None
            if sp_id in dict_sp_mcaq_data:
                dim_answer_mcaq = dict_sp_mcaq_data[sp_id]

            if dim_answer_likert is not None or dim_answer_oeq is not None or dim_answer_mcaq is not None:
                dict_sp_ans_data = {
                    'surveyParticipantId': sp_id,
                    'surveyInstanceId': survey_instance_id,
                    'dimSurveyInstanceId': dim_survey_instance_id,

                    'orgUnitId': org_id,
                    'dimOrgUnitId': dim_org_id,
                    'dimension': dict_sp_info[sp_id],

                    'dimAnswerLikert': dim_answer_likert,
                    'dimAnswerMCAQ': dim_answer_mcaq,
                    'dimAnswerOEQ': dim_answer_oeq,
                    'surveyDate': i['minCreatedDate'].strftime("%Y-%m-%d")
                }

                if sp_id in dict_pms.keys():
                    dict_sp_pms_data = {
                        'surveyParticipantId': sp_id,
                        'surveyInstanceId': survey_instance_id,
                        'dimSurveyInstanceId': dim_survey_instance_id,
                        'orgUnitId': org_id,
                        'dimOrgUnitId': dim_org_id,

                        'dimension': dict_sp_info[sp_id],
                        'dimMeasureScore': dict_pms[sp_id]['measureScore'],
                        'surveyDate': i['minCreatedDate'].strftime("%Y-%m-%d")
                    }
                else:
                    dict_sp_pms_data = None

                dict_sp_data = {
                    sp_id: {
                        'dimAnswer': dict_sp_ans_data,
                        'dimPMS': dict_sp_pms_data
                    }
                }

                lst_sp.append(dict_sp_data)
        return lst_sp

    @staticmethod
    def get_dimension_entry_for_participant(characteristic, dimension, i, dict_dimensions):
        if dict_dimensions.get(dimension).get(i[characteristic]) is None:
            dimension_id = 1
        else:
            dimension_id = dict_dimensions.get(dimension).get(i[characteristic])
        return dimension_id

    def get_ses_home_possession(self, survey_instance_id):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSESHomePossession)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql_result = self.cu_obj.run_select_query_survey_db(sql)

        dict_sp_ses_hp = dict()
        for i in sql_result:
            dict_sp_ses_hp[i['surveyParticipantId']] = i['score']

        return dict_sp_ses_hp

    # get the participant characteristic for gender, grade, ses, immigrant, aboriginal and age.Gets the
    # corresponding dimension table entries through get_dimension_entries() and returns them in a dictionary
    def get_participant_info(self, survey_instance_id, dim_survey_instance_id, dim_target_group_id, dict_sp_mcaq_data):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetParticipantCharacteristicIds)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql_result = self.cu_obj.run_select_query_survey_db(sql)

        characteristic_ids = None
        for char_id in sql_result:
            if characteristic_ids is not None:
                characteristic_ids = characteristic_ids + fc.FileConstants.COMMA_DELIMITER + str(char_id['characteristicId'])
            else:
                characteristic_ids = str(char_id['characteristicId'])

        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetSurveyParticipantInfo)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql_result = self.cu_obj.run_select_query_survey_db(sql)

        dict_dimensions = self.get_dimension_entries(dim_survey_instance_id, characteristic_ids)
        dict_sp_ses_hp = dict()
        if dim_target_group_id == dimTG.DimTargetGroupConstants.ELEMENTARY_SURVEY:
            dict_sp_ses_hp = self.get_ses_home_possession(survey_instance_id)

        dim_custom_measure = dict_dimensions.get('dimCustomMeasure')

        if sql_result is not None:
            dict_sp = {}
            for i in sql_result:

                if i['surveyParticipantId'] in dict_sp_ses_hp:
                    sp_ses_hp_char = dict_sp_ses_hp[i['surveyParticipantId']]
                else:
                    sp_ses_hp_char = 99

                if dict_dimensions.get('dimSESHP').get(sp_ses_hp_char) is None:
                    dim_ses_hp_id = 1
                else:
                    dim_ses_hp_id = dict_dimensions.get('dimSESHP').get(sp_ses_hp_char)

                dim_custom_measure_id = None

                if i['surveyParticipantId'] in dict_sp_mcaq_data:
                    sp_mcaq_data = dict_sp_mcaq_data[i['surveyParticipantId']]
                    lst_mcaq_ans_id = list()

                    for measure_id, answer in sp_mcaq_data.items():
                        for qn_id, ans_id in answer.items():
                            for key, value in ans_id.items():
                                if type(value) == int:
                                    lst_mcaq_ans_id.append(value)
                                elif type(value) == list:
                                    lst_mcaq_ans_id.extend(value)
                                elif value is None:
                                    lst_mcaq_ans_id.append(value)

                    for bridge_id, lst_drc_mcq_id in dim_custom_measure.items():
                        set_diff1 = set(lst_drc_mcq_id) - set(lst_mcaq_ans_id)
                        set_diff2 = set(lst_mcaq_ans_id) - set(lst_drc_mcq_id)
                        if len(set_diff1) == 0 and len(set_diff2) == 0:
                            dim_custom_measure_id = bridge_id
                            break
                    # print(i['surveyParticipantId'], ' -> ', dim_custom_measure_id)

                dict_sp_info = {
                    'dimGenderId': self.get_dimension_entry_for_participant('gender', 'dimGender', i, dict_dimensions),
                    'dimGradeId': self.get_dimension_entry_for_participant('grade', 'dimGrade', i, dict_dimensions),
                    'dimAgeId': self.get_dimension_entry_for_participant('age', 'dimAge', i, dict_dimensions),
                    'dimLangAtHomeId': self.get_dimension_entry_for_participant('langAtHome', 'dimLangAtHome', i, dict_dimensions),

                    'dimImmigrantId': self.get_dimension_entry_for_participant('immigrant', 'dimImmigrant', i, dict_dimensions),
                    'dimAboriginalId': self.get_dimension_entry_for_participant('aboriginal', 'dimAboriginal', i, dict_dimensions),
                    'dimGradeRepetitionId': self.get_dimension_entry_for_participant('gradeRepetition', 'dimGradeRepetition', i, dict_dimensions),
                    'dimEthnicityId': self.get_dimension_entry_for_participant('ethnicity', 'dimEthnicity', i, dict_dimensions),

                    'dimSESId': self.get_dimension_entry_for_participant('ses', 'dimSES', i, dict_dimensions),
                    'dimChangeSchoolId': self.get_dimension_entry_for_participant('changeSchool', 'dimChangeSchool', i, dict_dimensions),
                    'dimSpecialEducationId': self.get_dimension_entry_for_participant('specialEducation', 'dimSpecialEducation', i, dict_dimensions),
                    'dimFrenchImmersionId': self.get_dimension_entry_for_participant('frenchImmersion', 'dimFrenchImmersion', i, dict_dimensions),

                    'dimDisabilityId': self.get_dimension_entry_for_participant('disability', 'dimDisability', i, dict_dimensions),
                    'dimSESHPId': dim_ses_hp_id,
                    'dimCustMeasureId': dim_custom_measure_id,
                    'cellNum': i['cellNum']
                }

                dict_sp[i['surveyParticipantId']] = dict_sp_info
            return dict_sp
        else:
            return None

    def get_participant_measure_score(self, survey_instance_id, measure_ids, dict_sp_rep, dim_si_metadata):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetParticipantMeasureScores)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql = sql.replace('@pMeasureIds', measure_ids)
        lst_pms = self.cu_obj.run_select_query_survey_db(sql)

        dict_pms = dict()
        sp_id = None

        for i in lst_pms:
            tmp_sp_id = i['surveyParticipantId']
            if sp_id != tmp_sp_id:
                sp_id = tmp_sp_id
                measure_id = None

                for j in lst_pms:
                    if sp_id == j['surveyParticipantId']:
                        tmp_measure_id = j['measureId']
                        if measure_id != tmp_measure_id:
                            measure_id = tmp_measure_id
                            scaled_score = j['score']

                            dim_measure_id = dim_si_metadata['dictMeasures'][str(measure_id)]
                            cutoff = dim_si_metadata['dimMQS'][str(dim_measure_id)]['cutoff']
                            agg_rule_name = dim_si_metadata['dimMQS'][str(dim_measure_id)]['aggregateRuleName']
                            is_multi_cutoff = dim_si_metadata['dimMQS'][str(dim_measure_id)]['isMultiCutoff']

                            multi_cutoff1 = dim_si_metadata['dimMQS'][str(dim_measure_id)]['multiCutoff1']
                            multi_cutoff2 = dim_si_metadata['dimMQS'][str(dim_measure_id)]['multiCutoff2']

                            percent_score_mid = None
                            percent_score_high = None
                            replica_score = None
                            replica_percent_score = None
                            replica_scaled_score = None

                            if sp_id in dict_sp_rep and dict_sp_rep[sp_id] is not None \
                                    and measure_id in dict_sp_rep[sp_id] and dict_sp_rep[sp_id][measure_id] is not None:
                                replica_score = dict_sp_rep[sp_id][measure_id]

                            if agg_rule_name == 'cutoff':
                                if is_multi_cutoff == 0:
                                    if scaled_score >= cutoff:
                                        percent_score = 1
                                    else:
                                        percent_score = None

                                    replica_percent_score = replica_score
                                    # have to work on a logic to convert replica percent scores for mid and high. check this logic. seems wrong!

                                elif is_multi_cutoff == 1:
                                    if scaled_score >= multi_cutoff1:
                                        percent_score = 1
                                    else:
                                        percent_score = None

                                    if multi_cutoff1 <= scaled_score and scaled_score < multi_cutoff2:
                                        percent_score_mid = 1
                                    else:
                                        percent_score_mid = None

                                    if scaled_score >= multi_cutoff2:
                                        percent_score_high = 1
                                    else:
                                        percent_score_high = None

                                    replica_percent_score = replica_score
                                    # have to work on a logic to convert replica percent scores for mid and high. check this logic. seems wrong!

                            elif agg_rule_name == 'inverse_cutoff':
                                if scaled_score <= cutoff:
                                    percent_score = 1
                                else:
                                    percent_score = None

                                replica_percent_score = replica_score

                            elif agg_rule_name == 'mean':
                                percent_score = None
                                replica_scaled_score = replica_score

                            if sp_id in dict_pms:
                                tmp_pms = dict_pms[sp_id]['measureScore']
                                tmp_pms[dim_measure_id] = {
                                    'scaledScore': scaled_score,
                                    'percentScore': percent_score,
                                    'percentScoreMid': percent_score_mid,
                                    'percentScoreHigh': percent_score_high,

                                    'replicaScaledScore': replica_scaled_score,
                                    'replicaPercentScore': replica_percent_score,
                                    'replicaPercentScoreMid': None,
                                    'replicaPercentScoreHigh': None
                                }

                                dict_pms[sp_id]['measureScore'] = tmp_pms
                            else:
                                dict_pms[sp_id] = {
                                    'measureScore': {
                                        dim_measure_id: {
                                            'scaledScore': scaled_score,
                                            'percentScore': percent_score,
                                            'percentScoreMid': percent_score_mid,
                                            'percentScoreHigh': percent_score_high,

                                            'replicaScaledScore': replica_scaled_score,
                                            'replicaPercentScore': replica_percent_score,
                                            'replicaPercentScoreMid': None,
                                            'replicaPercentScoreHigh': None
                                        }
                                    }
                                }
        return dict_pms

    def get_replica_score_data(self, si_metadata):
        dict_sp_rep = dict()

        if si_metadata is not None:
            survey_instance_id = si_metadata.get('surveyInstanceId')
            target_group_id = si_metadata.get('dimTargetGroupId')  # have to include target Group id in si_metadata

            measure_ids = None
            for i in si_metadata['measureIds']:
                if measure_ids is None:
                    measure_ids = str(i)
                else:
                    measure_ids = measure_ids + fc.FileConstants.COMMA_DELIMITER + str(i)

            sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetReplicaScore)
            sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
            sql = sql.replace('@pMeasureIds', measure_ids)
            sql = sql.replace('@pTargetGroupId', str(target_group_id))
            sql = sql.replace('@pOrgIdForNorm', str(2))  # hardcoded norm id

            sql_result = self.cu_obj.run_select_query_survey_db(sql)
            for i in sql_result:
                if i['surveyParticipantId'] in dict_sp_rep:
                    tmp_dict_sp_rep = dict_sp_rep[i['surveyParticipantId']]
                    tmp_dict_sp_rep[i['measureId']] = i['cellMean']
                    dict_sp_rep[i['surveyParticipantId']] = tmp_dict_sp_rep
                else:
                    dict_sp_rep[i['surveyParticipantId']] = {
                        i['measureId']: i['cellMean']
                    }

        else:
            print('No valid survey instance metadata')

        return dict_sp_rep

    # populates the dimension entries from dim_gender, dim_grade, dim_ses, dim_ses_home_possession,
    # dim_immigrant_status and dim_aboriginal_status and returns them in a dictionary
    def get_dimension_entries(self, dim_survey_instance_id, characteristic_ids):
        # dim_gender
        sql = 'Select id, value From dim_gender Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_gender = self.populate_dict_dimension(sql)

        # dim_grade
        sql = 'Select id, value From dim_grade Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_grade = self.populate_dict_dimension(sql)

        # dim_age
        sql = 'Select id, value From dim_age Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_age = self.populate_dict_dimension(sql)

        # dim_lang_at_home
        sql = 'Select id, value From dim_lang_at_home Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_lang_at_home = self.populate_dict_dimension(sql)

        # dim_immigrant_status
        sql = 'Select id, value From dim_immigrant Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_imm = self.populate_dict_dimension(sql)

        # dim_aboriginal_status
        sql = 'Select id, value From dim_aboriginal Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_aborig = self.populate_dict_dimension(sql)

        # dim_grade_repetition
        sql = 'Select id, value From dim_grade_repetition Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_grade_repetition = self.populate_dict_dimension(sql)

        # dim_ses
        sql = 'Select id, value From dim_ses Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_ses = self.populate_dict_dimension(sql)

        # dim_ses_home_possession
        sql = 'Select id, value From dim_ses_hp Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_ses_hp = self.populate_dict_dimension(sql)

        # dim_disability
        sql = 'Select id, value From dim_disability Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_disability = self.populate_dict_dimension(sql)

        # dim_ethnicity
        sql = 'Select id, value From dim_ethnicity Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_ethnicity = self.populate_dict_dimension(sql)

        # dim_change_school
        sql = 'Select id, value From dim_change_school Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_change_school = self.populate_dict_dimension(sql)

        # dim_special_education
        sql = 'Select id, value From dim_special_education Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_special_education = self.populate_dict_dimension(sql)

        # dim_french_immersion
        sql = 'Select id, value From dim_french_immersion Where characteristicId In (@pCharacteristicIds) Or characteristicId IS NULL'
        sql = sql.replace('@pCharacteristicIds', characteristic_ids)
        dict_french_immersion = self.populate_dict_dimension(sql)

        # bridge table logic for mcq and maqs
        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetBridgeMcqByDimSurveyInstanceId)
        sql = sql.replace('@pDimSurveyInstanceId', str(dim_survey_instance_id))
        sql_result = self.cu_obj.run_select_query_reporting_db(sql)

        dict_bridge_mcaq = dict()
        for i in sql_result:
            str_drc_mcq_id = i['strDrcMcqId']
            arr_drc_mcq_id = str_drc_mcq_id.split(fc.FileConstants.COMMA_DELIMITER)
            lst_bridge = list()
            for element in arr_drc_mcq_id:
                if element == 'None':
                    element = None
                else:
                    element = int(element)
                lst_bridge.append(element)
            dict_bridge_mcaq[i['groupId']] = lst_bridge

        dict_dimensions = {
            'dimGender': dict_gender,
            'dimGrade': dict_grade,
            'dimAge': dict_age,
            'dimLangAtHome': dict_lang_at_home,

            'dimImmigrant': dict_imm,
            'dimAboriginal': dict_aborig,
            'dimGradeRepetition': dict_grade_repetition,
            'dimSES': dict_ses,

            'dimSESHP': dict_ses_hp,
            'dimDisability': dict_disability,
            'dimEthnicity': dict_ethnicity,
            'dimChangeSchool': dict_change_school,

            'dimSpecialEducation': dict_special_education,
            'dimFrenchImmersion': dict_french_immersion,
            'dimCustomMeasure': dict_bridge_mcaq
        }

        return dict_dimensions

    # checks if the value is 99 and returns None else the actual value.
    def populate_dict_dimension(self, sql):
        sql_result = self.cu_obj.run_select_query_reporting_db(sql)
        dict_dim = {}
        for dim in sql_result:
            if dim['value'] == 99 or dim['value'] == '99':
                val = None
            else:
                val = dim['value']
            dict_dim[val] = dim['id']
        return dict_dim

    @staticmethod
    def get_dict_rep_mqs(lst_mqs):
        dict_rep_mqs = dict()
        dim_measure_id = None
        for i in lst_mqs:
            tmp_dim_measure_id = i['dimMeasureId']

            if dim_measure_id is None or dim_measure_id != tmp_dim_measure_id:
                dim_measure_id = tmp_dim_measure_id
                dict_dim_measure = dict()
                dict_dim_measure[dim_measure_id] = i['measureId']
                dim_question_id = None
                dict_dim_question = dict()

                for j in lst_mqs:
                    tmp_dim_question_id = j['dimQuestionId']

                    if (dim_question_id is None or dim_question_id != tmp_dim_question_id) and dim_measure_id == j['dimMeasureId']:
                        dim_question_id = tmp_dim_question_id
                        dict_dim_question[dim_question_id] = {
                            dim_question_id: j['questionId']
                        }

                        dict_dim_lad = dict()
                        dim_lad_id = None
                        for k in lst_mqs:
                            tmp_dim_lad_id = k['dimLadId']

                            if (dim_lad_id is None or dim_lad_id != tmp_dim_lad_id) and dim_question_id == k['dimQuestionId']:
                                dim_lad_id = tmp_dim_lad_id
                                dict_dim_lad[dim_lad_id] = {
                                    dim_lad_id: k['likertAnswerDetailId'],
                                    'value': k['value'],
                                    'score': k['score'],
                                    'flagIncluded': k['flagIncluded']
                                }

                                dict_dim_question[dim_question_id]['dimLad'] = dict_dim_lad

                            dict_dim_measure['dimQuestion'] = dict_dim_question
                dict_rep_mqs[dim_measure_id] = dict_dim_measure

        return dict_rep_mqs

    @staticmethod
    def get_dict_mqs(lst_mqs):
        dict_mqs = dict()
        measure_id = None
        for i in lst_mqs:
            tmp_measure_id = i['measureId']

            if measure_id is None or measure_id != tmp_measure_id:
                measure_id = tmp_measure_id
                dict_measure = dict()
                dict_measure[measure_id] = i['dimMeasureId']
                question_id = None
                dict_question = dict()

                for j in lst_mqs:
                    tmp_question_id = j['questionId']

                    if (question_id is None or question_id != tmp_question_id) and measure_id == j['measureId']:
                        question_id = tmp_question_id
                        dict_question[question_id] = {
                            question_id: j['dimQuestionId']
                        }

                        dict_lad = dict()
                        lad_id = None
                        for k in lst_mqs:
                            tmp_lad_id = k['likertAnswerDetailId']

                            if (lad_id is None or lad_id != tmp_lad_id) and question_id == k['questionId']:
                                lad_id = tmp_lad_id
                                dict_lad[lad_id] = {
                                    lad_id: k['dimLadId'],
                                    'value': k['value'],
                                    'score': k['score'],
                                    'flagIncluded': k['flagIncluded']
                                }

                                dict_question[question_id]['lad'] = dict_lad

                            dict_measure['question'] = dict_question
                dict_mqs[measure_id] = dict_measure

        return dict_mqs

    def get_likert_answers(self, survey_instance_id, measure_ids, dim_si_metadata):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetAnswersLikert)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql = sql.replace('@pMeasureIds', measure_ids)

        lst_answers_oeq = self.cu_obj.run_select_query_survey_db(sql)
        sp_id = None
        dict_sp_oeq_data = dict()

        for i in lst_answers_oeq:
            tmp_sp_id = i['surveyParticipantId']

            if sp_id != tmp_sp_id:
                sp_id = tmp_sp_id
                dict_ans_data = dict()

                for j in lst_answers_oeq:
                    if sp_id == j['surveyParticipantId']:
                        dim_measure_id = dim_si_metadata['measureQuestion'][str(j['measureId'])][str(j['measureId'])]
                        dim_question_id = dim_si_metadata['measureQuestion'][str(j['measureId'])]['measureQuestion'][str(j['questionId'])]
                        dim_lad_id = dim_si_metadata['mqs'][str(j['measureId'])]['question'][str(j['questionId'])]['lad'][str(j['answerDetailId'])][str(j['answerDetailId'])]

                        if dim_measure_id in dict_ans_data:
                            tmp_dict_ans_data = dict_ans_data[dim_measure_id]['answer']
                            tmp_dict_ans_data[dim_question_id] = dim_lad_id

                            dict_ans_data[dim_measure_id] = {
                                'answer': tmp_dict_ans_data
                            }
                        else:
                            dict_ans_data[dim_measure_id] = {
                                'answer': {
                                    dim_question_id: dim_lad_id
                                }
                            }
                        dict_sp_oeq_data[sp_id] = dict_ans_data
        return dict_sp_oeq_data

    def get_open_ended_answers(self, survey_instance_id, measure_ids, dim_si_metadata):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetAnswersOEQ)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql = sql.replace('@pMeasureIds', measure_ids)

        lst_answers_oeq = self.cu_obj.run_select_query_survey_db(sql)
        lst_sp = list()
        sp_id = None
        dict_sp_oeq_data = dict()

        for i in lst_answers_oeq:
            tmp_sp_id = i['surveyParticipantId']

            if sp_id != tmp_sp_id:
                sp_id = tmp_sp_id
                dict_ans_data = dict()

                for j in lst_answers_oeq:
                    if sp_id == j['surveyParticipantId']:
                        # dim_measure_id = dim_si_metadata['mqs'][str(j['measureId'])][str(j['measureId'])]
                        # dim_question_id = dim_si_metadata['mqs'][str(j['measureId'])]['question'][str(j['questionId'])][str(j['questionId'])]
                        dim_measure_id = dim_si_metadata['measureQuestion'][str(j['measureId'])][str(j['measureId'])]
                        dim_question_id = dim_si_metadata['measureQuestion'][str(j['measureId'])]['measureQuestion'][str(j['questionId'])]

                        if dim_measure_id in dict_ans_data:
                            tmp_dict_ans_data = dict_ans_data[dim_measure_id]['answer']
                            tmp_dict_ans_data[dim_question_id] = j['answer']

                            dict_ans_data[dim_measure_id] = {
                                'answer': tmp_dict_ans_data
                            }
                        else:
                            dict_ans_data[dim_measure_id] = {
                                'answer': {
                                    dim_question_id: j['answer']
                                }
                            }
                        dict_sp_oeq_data[sp_id] = dict_ans_data
        return dict_sp_oeq_data

    def get_mcaq_answers(self, survey_instance_id, measure_ids, dim_si_metadata):
        sql = self.read_sql(self.SQL_PATH_SURVEY_DB + self.SQL_GetAnswersMCAQ)
        sql = sql.replace('@pSurveyInstanceId', str(survey_instance_id))
        sql = sql.replace('@pMeasureIds', measure_ids)
        lst_answers_mcaq = self.cu_obj.run_select_query_survey_db(sql)

        sql = self.read_sql(self.SQL_PATH_REPORTING_DB + self.SQL_GetDrcMcq)
        sql = sql.replace('@pDimSurveyInstanceId', str(dim_si_metadata['dimSurveyInstanceId']))
        lst_drc_mcq = self.cu_obj.run_select_query_reporting_db(sql)

        dict_drc_mcq = dict()
        for drc_mcq in lst_drc_mcq:
            dict_drc_mcq[drc_mcq['surveyMCQAnswerDetailId']] = drc_mcq['id']

        sp_id = None
        dict_sp_mcaq_data = dict()

        for i in lst_answers_mcaq:
            tmp_sp_id = i['surveyParticipantId']

            if sp_id != tmp_sp_id:
                sp_id = tmp_sp_id
                dict_ans_data = dict()

                for j in lst_answers_mcaq:
                    if sp_id == j['surveyParticipantId']:
                        dim_measure_id = dim_si_metadata['measureQuestion'][str(j['measureId'])][str(j['measureId'])]
                        dim_question_id = dim_si_metadata['measureQuestion'][str(j['measureId'])]['measureQuestion'][str(j['questionId'])]

                        if j['answerDetailId'] is not None:
                            dim_answer_detail_id = dict_drc_mcq[j['answerDetailId']]
                        else:
                            dim_answer_detail_id = None

                        if j['questionTypeId'] == 6:
                            if dim_measure_id in dict_ans_data:
                                tmp_dict_ans_data = dict_ans_data[dim_measure_id]['answer']
                                tmp_dict_ans_data[dim_question_id] = dim_answer_detail_id

                                dict_ans_data[dim_measure_id] = {
                                    'answer': tmp_dict_ans_data
                                }
                            else:
                                dict_ans_data[dim_measure_id] = {
                                    'answer': {
                                        dim_question_id: dim_answer_detail_id
                                    }
                                }
                            dict_sp_mcaq_data[sp_id] = dict_ans_data

                        elif j['questionTypeId'] == 7:
                            if dim_measure_id in dict_ans_data and dim_question_id in dict_ans_data[dim_measure_id]['answer']:
                                mul_qn = dict_ans_data[dim_measure_id]['answer'][dim_question_id]
                                mul_qn.append(dim_answer_detail_id)

                                dict_ans_data[dim_measure_id] = {
                                    'answer': {
                                        dim_question_id: mul_qn
                                    }
                                }
                            else:
                                dict_ans_data[dim_measure_id] = {
                                    'answer': {
                                        dim_question_id: [dim_answer_detail_id]
                                    }
                                }
                            dict_sp_mcaq_data[sp_id] = dict_ans_data
        return dict_sp_mcaq_data
