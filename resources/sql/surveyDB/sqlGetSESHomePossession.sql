-- set @pSurveyInstanceId = 113541;
Select DISTINCT surveyParticipantId, score
From ParticipantMeasureScores
Where surveyInstanceId = @pSurveyInstanceId And measureId = 1417
Order By surveyParticipantId;
