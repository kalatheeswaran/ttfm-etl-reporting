-- set @pSurveyInstanceId = 113541;
-- set @pMeasureIds = 1;
-- set @pTargetGroupId = 1;
-- set @pOrgIdForNorm = 2;
Select si.id as surveyInstanceId, sp.id as surveyParticipantId, rcm.measureId, rcm.cellMean
From SurveyInstance as si
Inner Join SurveyParticipant as sp On sp.surveyInstanceId = si.id
Inner Join ParticipantCharacteristic as pc On pc.surveyParticipantId = sp.id
Inner Join ReplicaCellMean as rcm On rcm.cellNum = pc.value

Inner Join
(Select rcm.measureId, MAX(rcm.yearTag) as yearTag
From ReplicaCellMean as rcm
Where rcm.normOrgId = @pOrgIdForNorm And rcm.targetGroupId = @pTargetGroupId And rcm.measureId In (@pMeasureIds)
Group By rcm.measureId) as yt
	On yt.measureId = rcm.measureId And yt.yearTag = rcm.yearTag

Where si.id = @pSurveyInstanceId And pc.characteristicId = 16 And rcm.normOrgId = @pOrgIdForNorm
	And rcm.targetGroupId = @pTargetGroupId And rcm.measureId In (@pMeasureIds)
Group By sp.id, rcm.measureId
Order By sp.id, rcm.measureId;
