Select DISTINCT si.id as surveyInstanceId, si.orgId as orgUnitId
From SurveyInstance as si
Inner Join ActionLog as al On al.surveyInstanceId = si.id
Where si.reportScheduled = 1 And si.reportGenerated = 0 And si.reportReleaseFlag = 0
	And al.actionId = 2 And si.id > 90000
Order By si.id