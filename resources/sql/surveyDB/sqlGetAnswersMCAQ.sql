-- set @pSurveyInstanceId = 106133;
-- set @pMeasureIds = 1;
Select sdm.measureId, sdm.questionId, sdm.questionTypeId, sdm.surveyParticipantId, a.answerDetailId, a.createdDate
From
(Select DISTINCT sdm.measureId, mq.questionId, qn.questionTypeId, sp.id as surveyParticipantId
From SurveyInstance as si
Inner Join SurveyDefinitionMeasure as sdm On sdm.surveyDefinitionId = si.surveyDefinitionId
Inner Join MeasureQuestion as mq On mq.measureId = sdm.measureId
Inner Join SurveyMCQ as smcq On smcq.surveyInstanceId = si.id And smcq.questionId = mq.questionId
Inner Join Question as qn On qn.id = mq.questionId
Inner Join SurveyParticipant as sp On sp.surveyInstanceId = si.id
Where si.id = @pSurveyInstanceId And qn.questionTypeId In (6,7)
-- And sp.id In (829184778,829184796)
And sdm.measureId In (@pMeasureIds)) as sdm

Left Join

(Select DISTINCT surveyParticipantId, questionId, answerDetailId, createdDate
From Answer USE INDEX(FK_Answer_3) Where surveyInstanceId = @pSurveyInstanceId) as a
On a.surveyParticipantId = sdm.surveyParticipantId And a.questionId = sdm.questionId

Order By sdm.surveyParticipantId, sdm.measureId, sdm.questionId, a.answerDetailId;
