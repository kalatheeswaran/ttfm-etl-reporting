-- set @pSurveyInstanceId = 113541;
-- set @pMeasureIds = 1;
Select DISTINCT a.surveyParticipantId, mq.measureId, a.questionId, qn.questionTypeId, a.answerDetailId, a.createdDate, lad.answer
From Answer as a USE INDEX(FK_Answer_3)
Inner Join Question as qn On qn.id = a.questionId
Inner Join MeasureQuestion as mq On mq.questionId = a.questionId
Inner Join OEQAnswerDetail as lad On lad.id = a.answerDetailId
Where a.surveyInstanceId = @pSurveyInstanceId And mq.measureId In (@pMeasureIds) And qn.questionTypeId In (2,3,9)
Order By a.surveyParticipantId, mq.measureId, a.questionId, a.answerDetailId;
