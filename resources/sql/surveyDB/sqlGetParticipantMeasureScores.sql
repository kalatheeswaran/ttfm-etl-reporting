-- set @pSurveyInstanceId = 97828;
-- set @pMeasureIds = 1;
Select DISTINCT pms.surveyInstanceId, pms.surveyParticipantId, pms.measureId, pms.score
From ParticipantMeasureScores as pms
Where pms.surveyInstanceId = @pSurveyInstanceId And pms.measureId In (@pMeasureIds)
Order By pms.surveyParticipantId, pms.measureId;
