-- have to change this sql on completion
-- set @pSurveyInstanceId = 97828;
Select DISTINCT si.id as surveyInstanceId, m.id as measureId, m.name as measureName
From SurveyInstance as si
Inner Join SurveyDefinition as sd On sd.id = si.surveyDefinitionId
Inner Join SurveyDefinitionMeasure as sdm On sdm.surveyDefinitionId = sd.id
Inner Join Measure as m On m.id = sdm.measureId
Where si.id = @pSurveyInstanceId And m.id Not In (1011,1013,1017,1061,1068,1119,1328,1378,1519,1652,1685, 1003,1004,1006,1023,1214,1049,1022,1029,1049,1521,1335)
Order By m.id;
