-- set @pSurveyInstanceId = 113541;
Select DISTINCT si.id as surveyInstanceId, mq.measureId, smcq.questionId, qn.questionTypeId, smcq.questionTitle, smcq.questionText,
smcq.drilldown, smcq.active, lad.id as answerDetailId, lad.label, lad.description, lad.value, lad.ordering
From SurveyInstance as si
Inner Join SurveyDefinitionMeasure as sdm On sdm.surveyDefinitionId = si.surveyDefinitionId
Inner Join Measure as m On m.id = sdm.measureId
Inner Join MeasureQuestion as mq On mq.measureId = sdm.measureId
Inner Join SurveyMCQ as smcq On smcq.surveyInstanceId = si.id And smcq.questionId = mq.questionId
Inner Join SurveyMCQAnswerDetail as lad On lad.surveyMCQId = smcq.id
Inner Join Question as qn On qn.id = smcq.questionId
Where si.id = @pSurveyInstanceId And m.reportable <> 0 And m.id Not In (1011,1013,1017,1061,1068,1119,1328,1378,1519,1652,1685, 1003,1004,1006,1023,1214,1049,1022,1029,1049,1521,1335)
Order By sdm.measureId, smcq.questionId, lad.id, lad.ordering;
