-- set @pSurveyInstanceId = 113541;
Select sp.id as surveyParticipantId,
MAX(CASE WHEN pc.characteristicId = 1 THEN CONVERT(pc.value, UNSIGNED INTEGER) ELSE NULL END) as gender,
MAX(CASE WHEN pc.characteristicId = 2 THEN CONVERT(pc.value, UNSIGNED INTEGER) ELSE NULL END) as grade,
MAX(CASE WHEN pc.characteristicId = 3 THEN CONVERT(pc.value, UNSIGNED INTEGER) ELSE NULL END) as age,
MAX(CASE WHEN pc.characteristicId = 16 THEN CONVERT(pc.value, UNSIGNED INTEGER) ELSE NULL END) as cellNum,

MAX(IF(pc.characteristicId In (21, 41, 60),
CASE WHEN pc.value In ('Eng+another', 'ang.+autre') THEN 'Eng+another'
	 WHEN pc.value In ('Eng+Fr', 'ang.+fran.') THEN 'Eng+Fr'
     WHEN pc.value In ('English', 'anglais', 'Eng') THEN 'English'
     WHEN pc.value In ('Other', 'autre', 'Oth') THEN 'Other'
     WHEN pc.value In ('Fr+another', 'fran.+autre') THEN 'Fr+another'
     WHEN pc.value In ('French', 'français') THEN 'French'
	 ELSE pc.value 
END, NULL)) as langAtHome,

MAX(IF(pc.characteristicId In (22,42),
CASE WHEN pc.value In ('<5YRS', '<5 ANS', '<5+ANS') THEN '<5YRS'	 
     WHEN pc.value In ('>5YRS', '>5 ANS', '>5+ANS') THEN '>5YRS'
	 ELSE pc.value 
END, NULL)) as immigrant,

MAX(IF(pc.characteristicId In (23, 35, 43),
CASE WHEN pc.value In ('Aborig', 'autoch.') THEN 'Aborig'	 
	 ELSE pc.value 
END, NULL)) as aboriginal,

MAX(IF(pc.characteristicId = 24,
CASE WHEN pc.value In ('Never', 'jamais') THEN 'Never'
	 WHEN pc.value In ('Once', 'une fois', 'une+fois') THEN 'Once'
     WHEN pc.value In ('Twice', 'deux fois', 'deux+fois') THEN 'Twice'
     ELSE pc.value
END, NULL)) as gradeRepetition,

MAX(CASE WHEN pc.characteristicId = 25 THEN pc.value ELSE NULL END) as ethnicity,
MAX(CASE WHEN pc.characteristicId = 46 THEN CONVERT(pc.value, UNSIGNED INTEGER) ELSE NULL END) as ses,

MAX(IF(pc.characteristicId = 52,
CASE WHEN pc.value In ('Moved', 'demenage') THEN 'Moved' 
	 WHEN pc.value In ('Expelled', 'expulse') THEN 'Expelled'	 
     WHEN pc.value In ('NoChg') THEN 'NoChg'
     WHEN pc.value In ('Program', 'programme') THEN 'Program'
	 ELSE pc.value 
END, NULL)) as changeSchool,

MAX(CASE WHEN pc.characteristicId = 54 THEN pc.value ELSE NULL END) as specialEducation,
MAX(CASE WHEN pc.characteristicId = 55 THEN pc.value ELSE NULL END) as frenchImmersion,
MAX(CASE WHEN pc.characteristicId In (56, 65) THEN pc.value ELSE NULL END) as disability

From SurveyParticipant as sp
Inner Join ParticipantCharacteristic as pc On pc.surveyParticipantId = sp.id
Where sp.surveyInstanceId = @pSurveyInstanceId
-- And sp.id In (829185191,829185074,829185306)
Group By sp.id
Order By sp.id
