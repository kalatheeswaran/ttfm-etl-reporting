-- set @pSurveyInstanceId = 113541;
Select sp.id as surveyParticipantId, MIN(a.createdDate) as minCreatedDate
From SurveyInstance as si
Inner Join SurveyParticipant as sp On sp.surveyInstanceId = si.id
Inner Join Answer as a USE INDEX(FK_Answer_3) On a.surveyInstanceId = si.id And a.surveyParticipantId = sp.id
Where si.id = @pSurveyInstanceId
-- And sp.id In (831103598,831103571,831103256)
-- And sp.id In (829185191,829185074,829185306)
-- And sp.id In (829184778,829184796)
-- And sp.id In (829397704, 829397631,829397610)
Group By sp.id
Order By sp.id
