-- set @pSurveyInstanceId = 113541;
Select DISTINCT pc.characteristicId
From SurveyParticipant as sp
Inner Join ParticipantCharacteristic as pc On pc.surveyParticipantId = sp.id
Where sp.surveyInstanceId = @pSurveyInstanceId
Order By pc.characteristicId;
