-- set @pSurveyInstanceId = 97828;
-- set @pMeasureIds = 1;
Select DISTINCT a.surveyParticipantId, mq.measureId, a.questionId, qn.questionTypeId, a.answerDetailId, a.createdDate
From Answer as a USE INDEX(FK_Answer_3)
Inner Join Question as qn On qn.id = a.questionId
Inner Join MeasureQuestion as mq On mq.questionId = a.questionId
Where a.surveyInstanceId = @pSurveyInstanceId And mq.measureId In (@pMeasureIds) And qn.questionTypeId = 1
Order By a.surveyParticipantId, mq.measureId, a.questionId, a.answerDetailId;
