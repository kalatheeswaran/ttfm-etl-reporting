-- set @pSurveyInstanceId = 97828;
-- set @pMeasureId = 1;
Select DISTINCT dgi.questionId, ql.id as questionLangId, ql.languageId, dgi.version, ql.questionHeader, ql.questionBody, ql.chartLabel
From SurveyInstance as si
Inner Join SurveyDefinition as sd On sd.id = si.surveyDefinitionId
Inner Join DisplayGroup as dg On dg.surveyDefinitionId = sd.id
Inner Join DisplayGroupPage as dgp On dgp.displayGroupId = dg.id
Inner Join DisplayGroupItem as dgi On dgi.pageId = dgp.id
Inner Join QuestionLang as ql On ql.questionId = dgi.questionId And ql.version = dgi.version
Where si.id = @pSurveyInstanceId And dgi.questionId In (Select DISTINCT mq.questionId From MeasureQuestion as mq Where mq.measureId In (@pMeasureId))
Order By ql.languageId, dgi.questionId;
