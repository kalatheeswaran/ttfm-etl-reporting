-- set @pSurveyInstanceId = 113541;
Select si.id, si.surveyDefinitionId, si.targetGroupId, si.yearDefId, si.snapshotId, a.startDate, a.endDate,
a.answerParticipantCount, pc.participantCharacteristicCount, pms.participantMeasureCount, si.surveyClosedFlag, si.reportScheduled,
si.reportGenerated, si.reportReleaseFlag, si.countryId, si.countryName, si.provinceId, si.provinceName,
si.districtId, si.districtName, si.schoolId, si.schoolName, si.clientId as schoolClientId, si.schoolDemo, b.id as brandId, b.brandPropertiesKey

From

(Select si.id, sd.id as surveyDefinitionId, sd.targetTypeCd as targetGroupId, si.yearDefId, si.snapshotId,
CONVERT(si.surveyClosedFlag, UNSIGNED INTEGER) as surveyClosedFlag,
CONVERT(si.reportScheduled, UNSIGNED INTEGER) as reportScheduled,
CONVERT(si.reportGenerated, UNSIGNED INTEGER) as reportGenerated,
CONVERT(si.reportReleaseFlag, UNSIGNED INTEGER) as reportReleaseFlag,
country.id as countryId, country.name as countryName, prov.id as provinceId, prov.name as provinceName,
dist.id as districtId, dist.name as districtName, sch.id as schoolId, sch.name as schoolName, sch.clientId, sch.demo as schoolDemo,
sch.brandPropertiesKey as schBrandKey, sd.propertiesKey as sdBrandKey,
IF(sch.brandPropertiesKey != NULL && sch.brandPropertiesKey != '', sch.brandPropertiesKey, sd.propertiesKey) as brandKey

From SurveyInstance as si
Inner Join SurveyDefinition as sd On sd.id = si.surveyDefinitionId
Inner Join OrganizationalUnit as sch On sch.id = si.orgId And sch.orgTypeId = 5
Inner Join OrganizationalUnit as dist On dist.id = sch.parentId
Inner Join OrganizationalUnit as prov On prov.id = dist.parentId
Inner Join OrganizationalUnit as country On country.id = prov.parentId
Where si.id = @pSurveyInstanceId) as si

Inner Join Brand as b On b.brandPropertiesKey = si.brandKey

Left Join

(Select a.surveyInstanceId, MIN(a.createdDate) as startDate, MAX(a.createdDate) as endDate,
count(DISTINCT a.surveyParticipantId) as answerParticipantCount
From Answer as a USE INDEX(FK_Answer_3)
Where a.surveyInstanceId = @pSurveyInstanceId) as a On a.surveyInstanceId = si.id

Left Join

(Select sp.surveyInstanceId, count(DISTINCT pc.surveyParticipantId) as participantCharacteristicCount
From SurveyParticipant as sp
Inner Join ParticipantCharacteristic as pc On pc.surveyParticipantId = sp.id
Where sp.surveyInstanceId = @pSurveyInstanceId) as pc On pc.surveyInstanceId = si.id

Left Join

(Select pms.surveyInstanceId, count(DISTINCT pms.surveyParticipantId) as participantMeasureCount
From ParticipantMeasureScores as pms
Where pms.surveyInstanceId = @pSurveyInstanceId) as pms On pms.surveyInstanceId = si.id
