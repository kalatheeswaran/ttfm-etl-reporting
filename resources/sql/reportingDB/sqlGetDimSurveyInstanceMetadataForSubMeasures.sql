-- set @pSurveyInstanceId = 113541;
-- set @pDimMeasureIds = 27;
Select DISTINCT si.surveyInstanceId, si.id as dimSurveyInstanceId, si.orgId as orgUnitId, si.dimOrgUnitId, si.surveyDefinitionId, si.dimTargetGroupId,
cm.measureId, cm.id as dimMeasureId, cm.measureName, m.measureTypeId, cm.defaultChartDataTypeId, cm.minimumAnswersRequired, cm.calcRuleName, cm.aggregateRuleName,
cm.minVal, cm.maxVal, cm.isMultiCutoff, cm.cutoff, cm.dispUnit,
JSON_EXTRACT(cm.multiCutoff, '$.cutoff1') as multiCutoff1, JSON_EXTRACT(cm.multiCutoff, '$.cutoff2') as multiCutoff2,
cm.parentDimMeasureId, cm.computeSubMeasures, cm.skipComputingMeasure,
JSON_EXTRACT(cm.childDimMeasureId, '$.id1') as childMeasureId1, JSON_EXTRACT(cm.childDimMeasureId, '$.id2') as childMeasureId2,
JSON_EXTRACT(cm.childDimMeasureId, '$.id3') as childMeasureId3, JSON_EXTRACT(cm.childDimMeasureId, '$.id4') as childMeasureId4,
JSON_EXTRACT(cm.childDimMeasureId, '$.id5') as childMeasureId5, JSON_EXTRACT(cm.childDimMeasureId, '$.id6') as childMeasureId6

From dim_measure as m 
Inner Join dim_measure as cm On cm.parentDimMeasureId = m.id
Cross Join 
(Select * From dim_survey_instance Where surveyInstanceId = @pSurveyInstanceId) as si
Where m.id In (@pDimMeasureIds) And m.computeSubMeasures = 1
Order By parentDimMeasureId, dimMeasureId
