-- set @pDimSurveyInstanceId = 546;
Select DISTINCT si.surveyInstanceId, sp.dimSurveyInstanceId, sp.surveyParticipantId, sp.id as dimSurveyParticipantId, sp.dimGenderId, sp.dimGradeId,
fact.dimMeasureId, fact.dimQuestionId, fact.dimLikertAnswerDetailId, fact.scaledScore
From dim_survey_instance as si
Inner Join dim_survey_participant as sp On sp.dimSurveyInstanceId = si.id
Inner Join fact_student_question_likert_score as fact On fact.dimSurveyInstanceId = si.id And fact.dimSurveyParticipantId = sp.id
Where si.id = @pDimSurveyInstanceId And fact.dimMeasureId In (1,16)
-- And sp.surveyParticipantId In (831103598,831103571)
Order By fact.dimMeasureId, fact.dimQuestionId, fact.dimLikertAnswerDetailId, sp.id;
