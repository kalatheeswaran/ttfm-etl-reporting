-- set @pDimSurveyInstanceId = 546;
Select DISTINCT si.surveyInstanceId, sp.dimSurveyInstanceId, sp.surveyParticipantId, sp.id as dimSurveyParticipantId, sp.dimGenderId, sp.dimGradeId,
fact.dimMeasureId, fact.scaledScore, fact.percentScore
From dim_survey_instance as si
Inner Join dim_survey_participant as sp On sp.dimSurveyInstanceId = si.id
Inner Join fact_student_measure_score as fact On fact.dimSurveyInstanceId = si.id And fact.dimSurveyParticipantId = sp.id
Where si.id = @pDimSurveyInstanceId
Order By fact.dimMeasureId, sp.id;
