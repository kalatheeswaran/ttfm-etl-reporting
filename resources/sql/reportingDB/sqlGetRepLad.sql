-- set @pDimMeasureIds = 1;
Select DISTINCT mq.measureId, mq.dimMeasureId, dimQ.questionId, dimQ.id as dimQuestionId, dimLad.likertAnswerDetailId as ladId, dimLad.id as dimLadId
From measure_question as mq
Inner Join dim_question as dimQ On dimQ.id = mq.dimQuestionId
Inner Join dim_lad as dimLad On dimLad.likertResponseGroupId = dimQ.responseGroupId
Where mq.dimMeasureId In (@pDimMeasureIds)
Order By mq.dimMeasureId, mq.dimQuestionId, dimLad.id;
