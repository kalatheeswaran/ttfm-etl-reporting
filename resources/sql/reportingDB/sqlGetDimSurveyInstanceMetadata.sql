-- set @pSurveyInstanceId = 113541;
Select DISTINCT si.surveyInstanceId, si.id as dimSurveyInstanceId, si.orgId as orgUnitId, si.dimOrgUnitId, si.surveyDefinitionId, si.dimTargetGroupId,
m.measureId, m.id as dimMeasureId, m.measureName, m.measureTypeId, m.defaultChartDataTypeId, m.minimumAnswersRequired, m.calcRuleName, m.aggregateRuleName,
m.minVal, m.maxVal, m.isMultiCutoff, m.cutoff, m.dispUnit,
JSON_EXTRACT(m.multiCutoff, '$.cutoff1') as multiCutoff1, JSON_EXTRACT(m.multiCutoff, '$.cutoff2') as multiCutoff2,
m.parentDimMeasureId, m.computeSubMeasures, m.skipComputingMeasure,
JSON_EXTRACT(m.childDimMeasureId, '$.id1') as childMeasureId1, JSON_EXTRACT(m.childDimMeasureId, '$.id2') as childMeasureId2,
JSON_EXTRACT(m.childDimMeasureId, '$.id3') as childMeasureId3, JSON_EXTRACT(m.childDimMeasureId, '$.id4') as childMeasureId4,
JSON_EXTRACT(m.childDimMeasureId, '$.id5') as childMeasureId5, JSON_EXTRACT(m.childDimMeasureId, '$.id6') as childMeasureId6

From dim_survey_instance as si
Inner Join survey_instance_measure as sim On sim.dimSurveyInstanceId = si.id
Inner Join dim_measure as m On m.id = sim.dimMeasureId
Where si.surveyInstanceId = @pSurveyInstanceId And m.skipComputingMeasure = 0 -- And m.measureId In (1)
-- And m.measureId Not In (1001,1002,1039,1040,1042,1045,1046,1047,1067,1513,1514,1518,1651)

UNION

Select DISTINCT si.surveyInstanceId, si.id as dimSurveyInstanceId, si.orgId as orgUnitId, si.dimOrgUnitId, si.surveyDefinitionId, si.dimTargetGroupId,
m.measureId, m.id as dimMeasureId, m.measureName, m.measureTypeId, m.defaultChartDataTypeId, m.minimumAnswersRequired, m.calcRuleName, m.aggregateRuleName,
m.minVal, m.maxVal, m.isMultiCutoff, m.cutoff, m.dispUnit,
JSON_EXTRACT(m.multiCutoff, '$.cutoff1') as multiCutoff1, JSON_EXTRACT(m.multiCutoff, '$.cutoff2') as multiCutoff2,
m.parentDimMeasureId, m.computeSubMeasures, m.skipComputingMeasure,
JSON_EXTRACT(m.childDimMeasureId, '$.id1') as childMeasureId1, JSON_EXTRACT(m.childDimMeasureId, '$.id2') as childMeasureId2,
JSON_EXTRACT(m.childDimMeasureId, '$.id3') as childMeasureId3, JSON_EXTRACT(m.childDimMeasureId, '$.id4') as childMeasureId4,
JSON_EXTRACT(m.childDimMeasureId, '$.id5') as childMeasureId5, JSON_EXTRACT(m.childDimMeasureId, '$.id6') as childMeasureId6

From dim_measure as m
Cross Join
(Select * From dim_survey_instance Where surveyInstanceId = @pSurveyInstanceId) as si
Where m.measureId In (1055,1056,1057, 1381,1385,1387,1388,1520, 1340,1383,1384,1393,1394,1395,1396) And m.skipComputingMeasure = 0

Order By parentDimMeasureId, dimMeasureId;
