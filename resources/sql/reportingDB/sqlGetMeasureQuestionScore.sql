-- set @pDimMeasureIds = 1;
Select DISTINCT mqs.measureId, m.id as dimMeasureId, mqs.questionId, q.id as dimQuestionId,
mqs.likertAnswerDetailId, lad.id as dimLadId, mqs.value, mqs.score, mqs.flagIncluded
From dim_measure as m
Inner Join measure_question_score as mqs On mqs.measureId = m.measureId
Inner Join dim_question as q On q.questionId = mqs.questionId
Inner Join dim_lad as lad On lad.likertAnswerDetailId = mqs.likertAnswerDetailId
Where m.id In (@pDimMeasureIds)
Order By mqs.measureId, mqs.questionId, mqs.likertAnswerDetailId;
