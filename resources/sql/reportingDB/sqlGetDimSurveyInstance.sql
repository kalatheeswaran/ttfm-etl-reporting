-- set @pSurveyInstanceId = 97828;
Select id, surveyInstanceId, dimOrgUnitId, orgId, surveyDefinitionId, dimTargetGroupId, yearDefId, dimBrandId, brandPropertiesKey
From dim_survey_instance
Where surveyInstanceId = @pSurveyInstanceId;
