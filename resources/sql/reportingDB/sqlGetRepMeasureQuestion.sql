-- set @pDimMeasureIds = 1;
Select DISTINCT mq.measureId, mq.dimMeasureId, mq.questionId, mq.dimQuestionId
From measure_question as mq
Where dimMeasureId In (@pDimMeasureIds)
Order By mq.dimMeasureId
