-- set @pDimMeasureIds = 1;
Select DISTINCT m.id as dimMeasureId, m.measureId, m.measureName, m.measureTypeId, m.defaultChartDataTypeId, mq.questionId, mq.dimQuestionId
From dim_measure as m
Inner Join measure_question as mq On mq.dimMeasureId = m.id
Where m.id In (@pDimMeasureIds)
Order By m.measureId, mq.questionId;
