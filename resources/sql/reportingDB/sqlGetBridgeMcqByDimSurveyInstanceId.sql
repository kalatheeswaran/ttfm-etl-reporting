-- set @pDimSurveyInstanceId = 387;
Select a.dimSurveyInstanceId, a.groupId, count(a.drcMcqId) as cnt, GROUP_CONCAT(a.drcMcqId SEPARATOR ',') as strDrcMcqId
From
(Select b.id, b.dimSurveyInstanceId, b.groupId, IF(b.drcMcqId IS NULL, "None", b.drcMcqId) as drcMcqId
From bridge_mcq as b
Where b.dimSurveyInstanceId = @pDimSurveyInstanceId) as a
Group By a.groupId
Order By a.groupId;
