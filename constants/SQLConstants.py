class SQLConstants:
    SQL_PATH_SURVEY_DB = 'resources/sql/surveyDB/'
    SQL_PATH_REPORTING_DB = 'resources/sql/reportingDB/'

    SQL_GetBatchSurveyInstances = 'sqlGetBatchSurveyInstances.sql'
    SQL_GetSurveyInstanceDetails = 'sqlGetSurveyInstanceDetails.sql'
    SQL_GetSurveyInstanceMeasures = 'sqlGetSurveyInstanceMeasures.sql'
    SQL_GetDisplayGroupQuestionLang = 'sqlGetDisplayGroupQuestionLang.sql'

    SQL_GetSurveyInstanceMetadata = 'sqlGetSurveyInstanceMetadata.sql'
    SQL_GetRepMeasureQuestion = 'sqlGetRepMeasureQuestion.sql'
    SQL_GetReplicaScore = 'sqlGetReplicaScore.sql'
    SQL_GetSurveyParticipantInfo = 'sqlGetSurveyParticipantInfo.sql'

    SQL_GetAnswersLikert = 'sqlGetAnswersLikert.sql'
    SQL_GetAnswersOEQ = 'sqlGetAnswersOEQ.sql'
    SQL_GetAnswersMCAQ = 'sqlGetAnswersMCAQ.sql'
    SQL_GetMeasureQuestionScore = 'sqlGetMeasureQuestionScore.sql'

    sqlGetRepLad = 'sqlGetRepLad.sql'
    SQL_GetParticipantMeasureScores = 'sqlGetParticipantMeasureScores.sql'
    SQL_GetSurveyMCQDetails = 'sqlGetSurveyMCQDetails.sql'
    SQL_GetSESHomePossession = 'sqlGetSESHomePossession.sql'

    SQL_GetParticipantCharacteristicIds = 'sqlGetParticipantCharacteristicIds.sql'
    SQL_GetSurveyParticipants = 'sqlGetSurveyParticipants.sql'

    SQL_GetDimSurveyInstance = 'sqlGetDimSurveyInstance.sql'
    SQL_GetDimQuestion = 'sqlGetDimQuestion.sql'
    SQL_GetDimQuestionByQuestionId = 'sqlGetDimQuestionByQuestionId.sql'
    SQL_GetDimMeasureFromMeasureId = 'sqlGetDimMeasureFromMeasureId.sql'

    SQL_GetDimMeasureFromDimMeasureId = 'sqlGetDimMeasureFromDimMeasureId.sql'
    SQL_GetDimTargetGroup = 'sqlGetDimTargetGroup.sql'
    SQL_GetDimBrand = 'sqlGetDimBrand.sql'
    SQL_GetDimSurveyInstanceDetails = 'sqlGetDimSurveyInstanceDetails.sql'

    SQL_GetDimSurveyInstanceMetadata = 'sqlGetDimSurveyInstanceMetadata.sql'
    SQL_GetDimSurveyInstanceMetadataForSubMeasures = 'sqlGetDimSurveyInstanceMetadataForSubMeasures.sql'
    SQL_GetDrcMcq = 'sqlGetDrcMcq.sql'
    SQL_GetBridgeMcqByDimSurveyInstanceId = 'sqlGetBridgeMcqByDimSurveyInstanceId.sql'

    SQL_GetFactStudentMeasureScore = 'sqlGetFactStudentMeasureScore.sql'
    SQL_GetFactStudentQuestionLikert = 'sqlGetFactStudentQuestionLikert.sql'
