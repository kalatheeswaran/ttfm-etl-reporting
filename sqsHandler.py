import utils.SQSUtils as sqs
import json
import sys


flag = int(sys.argv[1])
sqsUtils = sqs.SQSUtils()

if flag == 0:
    sqsUtils.purge_queue('tmpMaxInstances')
    sqsUtils.purge_queue('tmpSqsSurveyInstance')
    sqsUtils.purge_queue('etlRepSqsSurveyInstance')
    sqsUtils.purge_queue('etlRepSqsParticipantMeasureScore')
    sqsUtils.purge_queue('etlRepSqsParticipantQuestionScore')
    sqsUtils.purge_queue('etlRepDlqParticipantMeasureScore')
    sqsUtils.purge_queue('etlRepDlqParticipantQuestionScore')

elif flag == 1:
    lst_max_instances = list()
    lst_max_instances.append('1')
    res = sqsUtils.send_message('tmpMaxInstances', lst_max_instances)

    lst_survey_instance = list()
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 113541, 'orgUnitId': 2556})) # 2 mcqs
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 112529, 'orgUnitId': 7390})) # 2 mcq, 2 maq
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 108146, 'orgUnitId': 3340})) # 2 mcq, 2 maq
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 113056, 'orgUnitId': 6456}))  # 1 mcq
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 112124, 'orgUnitId': 12350}))  # 1 maq
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 114126, 'orgUnitId': 3984}))  # 2 maqs error
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 113299, 'orgUnitId': 12372}))  # 1 mcq, 1 maq error
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 108107, 'orgUnitId': 3294}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 107544, 'orgUnitId': 5920}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 113542, 'orgUnitId': 2556})) # elem for ses_hp
    res = sqsUtils.send_message('tmpSqsSurveyInstance', lst_survey_instance)
    print('Messages sent!')

elif flag == 10:
    lst_max_instances = list()
    lst_max_instances.append('10')
    res = sqsUtils.send_message('tmpMaxInstances', lst_max_instances)
    lst_survey_instance = list()
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 113541, 'orgUnitId': 2556}))

    lst_survey_instance.append(json.dumps({'surveyInstanceId': 108110, 'orgUnitId': 5904}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 106323, 'orgUnitId': 12214})) # error
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 108038, 'orgUnitId': 5980}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 112081, 'orgUnitId': 7488}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 107150, 'orgUnitId': 3284}))

    lst_survey_instance.append(json.dumps({'surveyInstanceId': 106586, 'orgUnitId': 3356}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 111587, 'orgUnitId': 3987}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 113036, 'orgUnitId': 12008}))
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 106133, 'orgUnitId': 7472}))  # error
    lst_survey_instance.append(json.dumps({'surveyInstanceId': 107088, 'orgUnitId': 3286}))  # error

    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 106009, 'orgUnitId': 12230}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 106323, 'orgUnitId': 12214}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 106338, 'orgUnitId': 7494}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 106586, 'orgUnitId': 3356}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 106882, 'orgUnitId': 5988}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 107544, 'orgUnitId': 5920}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 107942, 'orgUnitId': 4574}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 108038, 'orgUnitId': 5980}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 108110, 'orgUnitId': 5904}))
    # lst_survey_instance.append(json.dumps({'surveyInstanceId': 112081, 'orgUnitId': 7488}))
    res = sqsUtils.send_message('tmpSqsSurveyInstance', lst_survey_instance)
    print('Messages sent!')

elif flag == 20:
    # lst_survey_instance = list()
    # res = sqsUtils.send_message('tmpSqsSurveyInstance', lst_survey_instance)
    print('Messages sent!')

elif flag == 100:
    # lst_survey_instance = list()
    # res = sqsUtils.send_message('tmpSqsSurveyInstance', lst_survey_instance)
    print('Messages sent!')

elif flag == 654:
    # lst_survey_instance = list()
    # res = sqsUtils.send_message('tmpSqsSurveyInstance', lst_survey_instance)
    print('Messages sent!')

else:
    print('Enter flag value!')
